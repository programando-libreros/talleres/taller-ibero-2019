Srnicek, Nick y Williams, Alex. 2013. [_Accelerate Manifesto._](http://criticallegalthinking.com/2013/05/14/accelerate-manifesto-for-an-accelerationist-politics/)

Marx, Karl y Engels, Frederich. 2014. _Manifiesto del partido comunista_. Madrid: Fundación de Investigaciones Marxistas, 61.
 
Marx, Karl. 2014. “Fragment on Machines”, en _Accelerate_, editado por Robert Mackay y Armen Avanessian. Falmouth: Urbanomic Media Ltd., 63-4.

Deleuze, Gilles y Guattari, Félix. 1985. _El Anti Edipo_. _Capitalismo y esquizofrenia_. Barcelona: Paidós., 246-7
   
Lyotard, Jean-Francois. 1993. _Libidinal Economy_. Indiana: Indiana University Press., 241.
    
Noys, Benjamin. 2014. _Malign Velocities: Accelerationism and Capitalism_. Alresford: Zero Books, 34-5.
    
Laboria Cuboniks. [_Xenofeminism: A politics for alienation_](https://www.laboriacuboniks.net), 9-10.

