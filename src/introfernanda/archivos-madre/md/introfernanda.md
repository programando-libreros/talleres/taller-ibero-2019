#  La Moda ¿herrmaienta de liberación o de encadenamiento?

## Introducción


Si bien dentro de la historia de la filosofía sigue habiendo un rechazo de la moda, considerándola como un capricho ornamental del ser humano que no vale la pena investigar de modo filosófico, existen autores dentro del canon académico que se han dedicado a tratar el tema de la moda como un reflejo, casi inmediato, capaz de representar la identidad de un individuo o de un colectivo. 

La discusión sobre moda es cada vez más amplia e intrincada, en dónde el debate se extiende a la pregunta sobre la comprensión de este fenómeno. Existen dos posiciones antagónicas que nos ayudan a comprender el contexto en que la moda se puede llegar a considerar tema con relevancia filosófica. La primera posición señala, desde una concepción universalista, la raíces de la moda se encuentran en la naturaleza del ser humano como tal, mientras que, desde otra perspectiva se apunta que la esencia de la moda se encuentra intrínsecamente relacionada a la modernidad.@note Asimismo también se encuentra la discusión con respecto a si la moda puede ser considerada arte, o no.

La discusión de estas posturas se sostiene en la problemática respecto a pregunta de si la moda se puede localizar o no aún en el campo de la filosofía, pues desde una perspectiva tradicional, la moda es un campo que no recibe un detallado análisis ni evaluación de una colección con el mismo cuidado que se le dedica a cualquier arte, por lo que no se le considera aún en el campo académico.@note El filósofo noruego Lars Svendsen, en su ensayo “_On Fashion Criticism_”, argumenta que la moda no ha obtenido el mismo reconocimiento que otras formas de arte, dado a su falta de una critica seria.@note De forma similar, en el árticulo "_In defense of Fashion as a True Art Form_" en obeserver.com, Georges Berges señala que "One can tell as much about a culture by the paintings it produces as by the dresses and articles of clothing it uses for individual and collective expression.”@note De esta manera, podemos considerar la moda como una herramienta narrativa debido a que la moda va más allá de los estilos de ropa que se utilizan, sino que es una profunda expresión cultural que simultáneamente es individual y colectiva. 

### El valor filosfico de la moda  o  ALgunas perspectivas de la moda desde el estudio filosófico

### 
Los autores Giovanni Matteuci y Stefano Marino, en el texto Philosophical Perspectives of Fashion, hablan de la crítica continua que recibe la moda por su carácter frívolo, y cómo es que hasta recientemente, no se le atribuía valor  intelectual alguno.@note Sin embargo, Mateucci y Marino notan que el paso de la moda al ámbito académico predomina en enfoques sociológicos, psicológicos, antropológicos o estudios culturales. De esta manera, los autores se adentran a la discusión sobre el entendimiento de la moda y como se encuentra fuandamentada en la misma naturaleza humana y no en el epíritu de una época específica. 
 
>“In any case, it is out of doubt that fashion represents one of the cultural forms that perfectly embody the >heterogeneous, multiform, contradictory, to some extent perhaps superficial but also exciting tendencies of the present >age. From this point of view, fashion can be assumed as a mirror of the contemporary age and appears then of decisive >importance in order to gain insight into ourselves and the world we live in.”@note
		

Asimismo, Los autores señalan que la falta de un interés filosófico por la moda pueda ser por cómo se concibe a la naturaleza de la filosofía desde la tradición Occidental, principalmente enfocada a una actividad teórica que se concentra en la esencia de las cosas. "Conversely fashion has rather and always to do, because of its very nature, with surfaces, appearances, transience, and mere play of forms."@note Por lo tanto, Matteuci y Marino señalan que sigue exitiendo una brecha en el en la relción entre moda y filosofía, pues a pesar de que se le ha reconicido su reconocimiento en su estatus en la investigación científica.

### El dualismo 

El filósofo y sociólogo Georg Simmel, en su Hay una representación compleja de la identidad que se presenta en la moda, una especie de dualidad, o mejor dicho, una tensión que nos ayuda a comprender el provecho filosófico que proporciona el estudio de la moda.@note  

#### la tensión 


## Bibliografía / Referencias:

Berges, Georges. _Philosophical Perspectives of Fashion_ {.frances}

Lipovetsky, Gilles. 1996. _El imperio de lo efímero. La moda y sus destino en las sociedades modernas_. Barcelona: Anagrama. {.frances}

Simmel, Georg. _Fashion_ {.frances}

Matteuci, Marino. 2017. _Philosophical Perspectives on Fashion_ Oxford: Bloomsbury. {.frances}

De Perthuis, Karen. 2012. "I Am Style: Tilda Swinton as Emma in Luca Guadagnino's I Am Love" en _Film, Fashion & Consumption, vol. 1_ Intellect Ltd Article. {.frances}
