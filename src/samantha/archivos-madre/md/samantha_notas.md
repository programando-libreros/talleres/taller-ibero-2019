Linda M.G. Zerilli, “A Process without a Subject: Simone de Beauvoir and Julia Kristeva on Maternity”, Sings, Vol. 18, No. 1, otoño, 1992. 132.
 
María Laura Giallorenzi, “La maternidad como proyecto individual y autónomo. El caso de las madres solas por elección”, Journal de Ciencias Sociales, Año 6, No. 11, 2018. 141-143.
  
Gayatri Chakravorty Spivak “¿Puede hablar el subalterno?” Revista Colombiana de Antropología, Vol. 39, enero-diciembre 2003. 307.
   
Stephen Morton, “Las mujeres del «tercer mundo» y el pensamiento occidental”,  La manzana de la discordia, Vol. 5, No. 1, enero-junio 2010. 117.  

Elisabeth Badinter, ¿Existe el amor maternal? Historia del amor maternal. Siglos XVII al XX, (Barcelona: Paidós/Pomaire, 1981). 309. 

Rosario Castellanos, Sobre cultura femenina, (Ciudad de México: Fondo de Cultura Económica, 2005) 215-216. 
