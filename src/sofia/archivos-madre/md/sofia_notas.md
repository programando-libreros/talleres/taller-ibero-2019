Max Horkheimer, Crítica de la Razón Instrumental (Buenos Aires: SUR ,1973)185. 

Fernando J. Vergara Herníquez, “La modernidad revelada o la crítica ante la irrenunciabilidad al progreso en Adorno y Horkheimer”, Revista Opción 65 (septiembre-diciembre 2011): 93-115.

Stephanie Graf, “La actualidad de los “Elementos del Antisemitismo” de Adorno y Horkheimer”, Revista Signos Filosóficos 38 (julio-diciembre 2017) 118-149.

Amy Allen, “Emancipación sin utopía: Sometimiento, modernidad y las reivindicaciones normativas de la teoría crítica feminista”, Revista Signos Filosóficos 35 (enero-junio 2016):170-196.

Joseph Rouse, “The Dynamics of Power an Knowledge in Science”, The Journal of Philosophy 11 (noviemnre1991): 658-665.

L. Alcoff, “Epistemologies of Ignorance: Three types”, en Race and Epistemologies of Ignorance, Sullivan Shannon y Nancy Tuana., ed. (E.U.A: State University of New York 2007)39-49.

Francisco Daniel Tiapa-Blanco, “Colonialismo, Miradas fronterizas y denaturalización de los sustratos epistemológicos del eurocentrismo”, Revista LiminaR. Estudios Sociales y Humanísticos 1 (enero-junio 2019) ):114-126.

Fernando J. Vergara Herníquez, “La modernidad revelada o la crítica ante la irrenunciabilidad al progreso en Adorno y Horkheimer”, Revista Opción 65 (septiembre-diciembre 2011): 97.

Fernando J. Vergara Herníquez, “La modernidad revelada o la crítica ante la irrenunciabilidad al progreso en Adorno y Horkheimer”, Revista Opción 65 (septiembre-diciembre 2011): 99.

Fernando J. Vergara Herníquez, “La modernidad revelada o la crítica ante la irrenunciabilidad al progreso en Adorno y Horkheimer”, Revista Opción 65 (septiembre-diciembre 2011): 106.

Stephanie Graf, “La actualidad de los “Elementos del Antisemitismo” de Adorno y Horkheimer”, Revista Signos Filosóficos 38 (julio-diciembre 2017) 124.

Amy Allen, “Emancipación sin utopía: Sometimiento, modernidad y las reivindicaciones normativas de la teoría crítica feminista”, Revista Signos Filosóficos 35 (enero-junio 2016): 173-174. 

Amy Allen, “Emancipación sin utopía: Sometimiento, modernidad y las reivindicaciones normativas de la teoría crítica feminista”, Revista Signos Filosóficos 35 (enero-junio 2016): 179 .

Joseph Rouse, “The Dynamics of Power an Knowledge in Science”, The Journal of Philosophy 11 (noviemnre1991): 660.

L. Alcoff, “Epistemologies of Ignorance: Three types”, en Race and Epistemologies of Ignorance, Sullivan Shannon y Nancy Tuana., ed. (E.U.A: State University of New York 2007):51.

L. Alcoff, “Epistemologies of Ignorance: Three types”, en Race and Epistemologies of Ignorance, Sullivan Shannon y Nancy Tuana., ed. (E.U.A: State University of New York 2007):57.

Francisco Daniel Tiapa-Blanco, “Colonialismo, Miradas fronterizas y denaturalización de los sustratos epistemológicos del eurocentrismo”, Revista LiminaR. Estudios Sociales y Humanísticos 1 (enero-junio 2019) ):118.
