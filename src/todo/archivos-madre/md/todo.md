# Conceptos y percepción: un análisis de la experiencia humana

## Introducción:

El presente dosier tiene como objetivo fundamental el llevar
a cabo la contextualización de los términos propios en los que
se desarrolla la discusión filosófica entre conceptualistas y
no-conceptualistas. Si bien se trata de un preámbulo motivado
por el interés en llevar a cabo la exposición de un caso en defensa
de la primera de estas posturas, la selección de textos expuesta
a continuación pretende ejercer como un menú de lectura variado,
que capacite a cualquier lector con las herramientas necesaria
para introducirse en el debate en cuestión.

### I.	La disyuntiva kantiana entre conceptos e intuiciones:

La irrupción del pensamiento kantiano en el marco de la discusión
moderna entre racionalistas y empiristas dejó una huella determinante
en el proceso de configuración de la epistemología como la conocemos
en la actualidad. El motivo detrás de esto puede encontrarse
en la adopción de una actitud conciliadora por parte de Kant,
materializada en su esfuerzo por sintetizar los elementos pertinentes
de cada postura en un mismo sistema. Quizás, uno de los puntos
de la obra kantiana en donde mejor puede apreciarse el sentido
de semejante actitud se ubique en la introducción general al
capítulo de la Lógica trascendental, en la Crítica de la razón
pura. Me refiero a aquel pasaje paradigmático en donde el filósofo
de Königsberg manifiesta que “los pensamientos sin contenido
son vacíos y las intuiciones sin conceptos son ciegas”.

### II.	La traducción del problema en términos de espontaneidad y receptividad:

En la contemporaneidad, al intentar rastrear la herencia de aquel
planteamiento nos dirigimos irremediablemente hacia las teorías
conceptualistas en filosofía analítica. En concreto, el libro
_Mente y mundo_ del filósofo sudafricano John McDowell, se presenta
como una entrada determinante a esta línea de pensamiento. Para
los fines aquí perseguidos, hemos apelado a un fragmento que
se despliega en torno a la necesidad de un cierto tipo de interpretación
respecto a la disyuntiva kantiana mencionada en el apartado anterior,
una interpretación centrada en la analogía entre las dualidades
concepto-intuición y espontaneidad-receptividad. Pues es precisamente
con base en esta segunda caracterización, que resulta factible
llevar a cabo la expansión del marco de facultades posiblemente
atribuibles a cada una de estas categorías. La idea de fondo,
en apego a la dinámica de subsunción tan presente a lo largo
de la primera _Crítica_, apunta a que la sensibilidad pueda ser
identificada como un mecanismo conformado por un conjunto de
operaciones inherentes a la espontaneidad, es decir, a las actividades
de corte conceptual.

### III. Motivaciones no-conceptualistas:

En la búsqueda de lecturas que permitan acceder a la causa no
conceptualista, el artículo _Kantian non-conceptualism_ del filósofo
canadiense Robert Hanna resulta de gran utilidad. En el fragmento
de este texto aquí recuperado, se aprecian cuando menos un par
de aportaciones sustanciales. Primero, la delimitción de la postura
no-conceptualista como aquella evocada a la defensa de la tesis
que afirma las siguientes dos premisas: a) “el contenido representacional
no está ni única ni exclusivamente determinado por nuestras capacidades
conceptuales” y b) “por lo menos algunos contenidos están tanto
única como exclusivamente determinados por capacidades no-conceptuales,
de manera que pueden ser compartidos por humanos y animales no-humanos
por igual” . Al reparar en las implicaciones de la tesis nos
percatamos de que para Hanna, el asunto en juego en el debate
entre conceptualistas y no-conceptualistas se concentra fundamentalmente
en el concepto de “contenido representacional”. Asimismo, puede
observarse la introducción de un componente biológico al debate,
ilustrado en la distinción entre humanos y animales no-humanos,
como los dos géneros de sujeto sobre los que se cimienta la investigación.
La segunda aportación de relevancia por parte de Hanna, es la
demarcación de dos razones por las que podría resultar pertinente
adoptar una perspectiva no-conceptualista. La primera de estas
es el interés en ponderar “una forma de realismo perceptual directo”,
con lo cual, indirectamente, se deposita la problemática etiqueta
de metafísica sobre el conceptualismo.

### IV.	La unidad de la apercepción como respuesta a Hanna:

El cuarto trabajo recuperado para el presente dosier, es el artículo
_A Conceptualist Reply to Hanna’s Kantian Non-Conceptualism_
de Brady Bowman. Aquí, como el título mismo lo indica, con lo
que nos encuentramos es con una suerte de respuesta conceptualista
a los argumentos expuestos por Hanna, fundanda en la demostración
de su discordancia con los objetivos marcados por Kant en la
_Analítica trascendental_. En términos más específicos, el eje
del fragmento seleccionado, parte de la identificación de que
“dos consecuencias que se siguen de la interpretación de Hanna
[…] resultan fundamentalmente incompatibles con el espíritu y
esfuerzo de la filosofía crítica de Kant”.

#  La Moda ¿herrmaienta de liberación o de encadenamiento?

## Introducción


Si bien dentro de la historia de la filosofía sigue habiendo un rechazo de la moda, considerándola como un capricho ornamental del ser humano que no vale la pena investigar de modo filosófico, existen autores dentro del canon académico que se han dedicado a tratar el tema de la moda como un reflejo, casi inmediato, capaz de representar la identidad de un individuo o de un colectivo. 

La discusión sobre moda es cada vez más amplia e intrincada,
en dónde el debate se extiende a la pregunta sobre la comprensión
de este fenómeno. Existen dos posiciones antagónicas que nos
ayudan a comprender el contexto en que la moda se puede llegar
a considerar tema con relevancia filosófica. La primera posición
señala, desde una concepción universalista, la raíces de la moda
se encuentran en la naturaleza del ser humano como tal, mientras
que, desde otra perspectiva se apunta que la esencia de la moda
se encuentra intrínsecamente relacionada a la modernidad.@note
Asimismo también se encuentra la discusión con respecto a si
la moda puede ser considerada arte, o no.

La discusión de estas posturas se sostiene en la problemática
respecto a pregunta de si la moda se puede localizar o no aún
en el campo de la filosofía, pues desde una perspectiva tradicional,
la moda es un campo que no recibe un detallado análisis ni evaluación
de una colección con el mismo cuidado que se le dedica a cualquier
arte, por lo que no se le considera aún en el campo académico.@note
El filósofo noruego Lars Svendsen, en su ensayo “_On Fashion
Criticism_”, argumenta que la moda no ha obtenido el mismo reconocimiento
que otras formas de arte, dado a su falta de una critica seria.@note
De forma similar, en el árticulo "_In defense of Fashion as a
True Art Form_" en obeserver.com, Georges Berges señala que "One
can tell as much about a culture by the paintings it produces
as by the dresses and articles of clothing it uses for individual
and collective expression.”@note De esta manera, podemos considerar
la moda como una herramienta narrativa debido a que la moda va
más allá de los estilos de ropa que se utilizan, sino que es
una profunda expresión cultural que simultáneamente es individual
y colectiva.

### El valor filosfico de la moda  o  ALgunas perspectivas de la moda desde el estudio filosófico

### 
Los autores Giovanni Matteuci y Stefano Marino, en el texto Philosophical Perspectives of Fashion, hablan de la crítica continua que recibe la moda por su carácter frívolo, y cómo es que hasta recientemente, no se le atribuía valor  intelectual alguno.@note Sin embargo, Mateucci y Marino notan que el paso de la moda al ámbito académico predomina en enfoques sociológicos, psicológicos, antropológicos o estudios culturales. De esta manera, los autores se adentran a la discusión sobre el entendimiento de la moda y como se encuentra fuandamentada en la misma naturaleza humana y no en el epíritu de una época específica. 
 
> “In any case, it is out of doubt that fashion represents one of the cultural forms that perfectly embody the >heterogeneous, multiform, contradictory, to some extent perhaps superficial but also exciting tendencies of the present >age. From this point of view, fashion can be assumed as a mirror of the contemporary age and appears then of decisive >importance in order to gain insight into ourselves and the world we live in.”@note
		

Asimismo, Los autores señalan que la falta de un interés filosófico
por la moda pueda ser por cómo se concibe a la naturaleza de
la filosofía desde la tradición Occidental, principalmente enfocada
a una actividad teórica que se concentra en la esencia de las
cosas. "Conversely fashion has rather and always to do, because
of its very nature, with surfaces, appearances, transience, and
mere play of forms."@note Por lo tanto, Matteuci y Marino señalan
que sigue exitiendo una brecha en el en la relción entre moda
y filosofía, pues a pesar de que se le ha reconicido su reconocimiento
en su estatus en la investigación científica.

### El dualismo 

El filósofo y sociólogo Georg Simmel, en su Hay una representación
compleja de la identidad que se presenta en la moda, una especie
de dualidad, o mejor dicho, una tensión que nos ayuda a comprender
el provecho filosófico que proporciona el estudio de la moda.@note

#### la tensión 

## Bibliografía / Referencias:

Berges, Georges. _Philosophical Perspectives of Fashion_ {.frances}

Lipovetsky, Gilles. 1996. _El imperio de lo efímero. La moda
y sus destino en las sociedades modernas_. Barcelona: Anagrama.
{.frances}

Simmel, Georg. _Fashion_ {.frances}

atteuci, Marino. 2017. _Philosophical Perspectives on Fashion_
Oxford: Bloomsbury. {.frances}

De Perthuis, Karen. 2012. "I Am Style: Tilda Swinton as Emma
in Luca Guadagnino's I Am Love" en _Film, Fashion & Consumption,
vol. 1_ Intellect Ltd Article. {.frances}

# Por un materialismo de la mente más allá del cerebro

## Introducción

El siguiente dosier tiene una estructura expositiva que tiene
como principal objetivo conectar el _debate_ mente-cerebro y
la postura materialista, con la importancia y validez de aquello
que existe de manera externa en términos sociales y materiales.
El debate filosófico contemporáneo entre mente y cuerpo sigue
presente en el siglo +++XXI+++. Todavía no se encuentra una solución
para dar cuenta de la _existencia inmaterial_ de los estados
mentales y su relación con la materialidad física del cuerpo
y de aquello que lo rodea. Es evidente que no podemos pasar por
alto la discusión, ni siquiera temporalmente. Es decir, la pregunta
por la mente y el cuerpo lleva ya vigente unos cinco siglos,
desde las meditaciones hechas por Descartes hasta el dualismo
de propiedad de Chalmers y muchos pensadores más.

La extensión del problema mente-cuerpo no nos permite más que
abordarlo por especificidad. Dado que lo general, en términos
de tradición filosófica, es el problema de relación entre lo
mental y lo material o físico, lo específico radicaría en la
postura que se tome para dar cuenta de dicha relación. En las
discusiones recientes, diversos pensadores han propuesta y argumentado
distintas soluciones al problema, pero ninguna se ha posicionado
como la vencedora. Muy probablemente porque el valor del problema
yace en la luz que cada explicación pudiera brindar a la generalidad
del problema. Aunque también habría que considerar que las soluciones
propuestas ponen en primacía ciertas propiedades u objetos que
se consideran demasiado evidentes y que condicionan dichas explicaciones.
Una reducción de la mente a los confines materiales del cerebro,
dado que es evidente que sin cerebro no hay mente, sería el mejor
ejemplo de esto.

En lo que va del siglo XXI, la postura filosófica predominante
en el debate mente-cuerpo es el materialismo. La mente, en principio,
debería reducirse a los procesos físicos del cerebro y esto implica
que, en principio, se podría dar cuenta de los estados mentales
a partir de la interacción específica de neuronas y las partes
del cerebro que los producen. La condición presupuesta de esto
es que no se puede concebir una mente sin el sustento físico
de un cerebro. Por lo tanto, ya se presupone para cualquier explicación
materialista de la mente, la existencia física de un cerebro
que pueda sustentar todo lo demás.

Sin embargo, no se ha llegado a una respuesta que logre probar
sin lugar a duda cuáles son las partes físicas específicas que
producen estados mentales específicos, y mucho menos el paso
de lo físico a las experiencias subjetivas. Entonces, cabe la
crítica sobre sí de hecho y desde una postura materialista, tiene
que ser siempre el cerebro el presupuesto central para cualquier
explicación de la misma índole.

No podemos negar que sin cerebro no habría mente, pero la explicación
de la misma podría no reducirse solamente a los procesos físicos
del cerebro. Procesos que son sólo internos y que ocurren dentro
del cráneo, sin una interacción propiamente externa. Parece que
el salto de lo físico a lo mental sólo tiene cabida en la interacción
eléctrica de neuronas en conjunto a las partes cerebrales desde
donde actúan y comunican. Aunque esto no anula la posibilidad
de otro elemento que ayude a la explicación interna del cerebro
y la mente, un elemento acorde a la postura materialista pero
fuera del propio cerebro.

¿Se podría dar una explicación plausible y de corte materialista
de la mente a partir de las interacciones externas que tiene?
¿Podría ser que la interacción externa del cerebro y el cuerpo
con aquello que lo rodea, tenga la misma importancia que las
interacciones internas del cerebro y sus partes? El enfoque tendría
que ser no sólo a partir del cerebro como órgano, sino que también
en su relación corporal y externa con un mundo que se rige por
lo social y cultural. No podría dejarse de lado la evolución
del cerebro y en último término el de la mente sin su relación
externa con el mundo. Por lo tanto, lo externo al cerebro en
tanto que material, podría dar preguntas y respuestas que competen
al debate entre mente-cerebro y la relación que ambos tienen.

### Los límites del debate y el materialismo

El debate contemporáneo sobre la mente y el cuerpo oscilan entre
dos preguntas fundamentales:

>In late twentieth century philosophy of mind, discussions of the mind-body problem revolve around the twin poles of the problem of psychophysical causation >and the problem of consciousness. And while it is possible to see these as independent problems, there is nonetheless a link between them, which can be >expressed as a dilemma: if the mental is not physical, then how we make sense of its causal interaction with the physical? But if it is physical, how can we >make sense of the phenomena of consciousness? These two questions, in effect, define the contemporary debate on the mind-body problem.@note  

El texto por Tim Crane nos ayuda a definir cuál es el enfoque
en el que se da la discusión contemporánea entre mente y cerebro.
De igual forma, nos define de una manera bastante simple y directa
la problemática en tanto que la imposibilidad para dar cuenta
entre la causa física de los estados mentales y, si lo son, cuál
sería el brinco entre los procesos físicos y la conciencia como
tal. Es ampliamente informativo e ilustrativo en referencia a
los diferentes desarrollos del tema en general y sus posibles
bifurcaciones.

Con el fin de especificar aún más, faltaría definir nuestra postura.
Ya se vio que la físico-materialista predomina en el debate,
pero ¿qué es una postura materialista en filosofía?

>El materialismo antropológico se centra en explicar la naturaleza humana a partir de sus componentes físicos o fisiológicos. Al distinguir tajantemente la >´res cogitans´ (o alma) de la ´res extensa´ o cuerpo, Descartes contribuyó directa (aunque involuntaria) a la difusión del materialismo antropológico de la >modernidad. La tesis de la materialidad del alma fue tópica en la literatura clandestina de los libertinos y sirvió como punta de lanza de la Ilustración >contra la tradición cristiana. Los descubrimientos médicos y fisiológicos que mostraban la dependencia de las funciones espirituales respecto de sus >condiciones anatómicas y orgánicas, permitió al médico J.O de La Mettrie trazar una historia natural del alma (1745) y formular la famosa tesis del ´hombre >máquina´ y a David Hartley defender la indisociabilidad del pensamiento y la sensación (1749).@note


### El dominio material del cerebro en contraposición a sus límites externos

El materialismo, según la definición de Alberto Tuñón, radica
en lo físico como unidad fundamental de explicación del humano
y por tanto de su mente. No sólo esto, también desarrolla distintas
definiciones del materialismo en distintas disciplinas y áreas
filosóficas que pueden ofrecer mucha más información histórica
y su desarrollo específico. Esto va de la mano con la mirada
que se le ha dado a la mente como tal. La ciencia en su dominio
de lo físico y natural, no consideraba a la mente como parte
de su estudio.

>Sin embargo, la conciencia no es una cosa; es, para Searle, una propiedad o rasgo del cerebro, al igual que la liquidez es una propiedad del agua. Esta visión >del problema contrasta con lo que ha sido su desarrollo histórico desde el siglo XVII, cuando Descartes, Galileo y otros pensadores de la época excluyeron a >la conciencia como materia propia de la ciencia. La ciencia natural no podía estudiar la mente, (la res cogitans), debía interesarse únicamente de la materia >(res extensa). Aunque esto ha facilitado progresos importantes en el campo de la ciencia, filosóficamente ha constituido un grave obstáculo en nuestros días >para la comprensión científica del lugar que la conciencia tiene en el mundo natural.@note

Se relegó a la filosofía la explicación de la interrogante que
producía la mente humana y ésta no era materialista. La ciencia
no aplicaba sus métodos a la mente y la filosofía intentaba atacar
el problema desde diversos frentes. Lo que resultó en muchas
conclusiones pero ninguna con los planteamientos propios de la
ciencia. Es sólo en la época contemporánea, a partir del siglo
XXI, que se plantea a la mente y sus características en término
científicos, en específico con la neurociencia.

Los resultados y análisis que el texto de Velasco plantea van
más allá de la neurociencia cuando ésta afirma la imperante e
incondicional relación que existe entre el cerebro dentro del
cráneo como fuera del mismo. La interacción con aquello que lo
rodea es imprescindible para el desarrollo neuronal. Su mundo
exterior provee una dirección distinta de lo que se entiende
por lo mental y éste no puede ser a partir de la neurociencia.
La electricidad de la neuronas y las funciones de las partes
del cerebro no aportan nada a lo externo. Tendríamos que tener
una serie de conceptos que puedan describir las relaciones de
la mente y el cerebro con lo que los rodea. Ya sean objetos,
seres vivos, otras personas o la naturaleza misma como ecosistema.
Por ello me remito al umwelt y la semiosfera.

>Umwelt is the semiotic world of organism. It includes all the meaningful aspects of the world for a particular organism. Thus, Umwelt is a term uniting all >the semiotic processes of an organism into a whole. Indeed, the Umwelt-concept follows naturally due to the connectedness of individual semiotic processes >within an organism, which means that any individual semiosis in which an organism is functioning as a subject is continuously connected to any other semiosis >of the same organism. At the same time, the Umwelts of different organisms differ, which follows from the individuality and uniqueness of the history of every >single organism. Umwelt is the closed world of organism. The functional closer, or epistemic closer is an important and principal feature of organisms, and of >semiotic systems. This has been described by Maturana and Varela (1980) through the notion of autopoiesis.@note

Kalevi Kull nos brinda un desarrollo casi de comentario alrededor
del concepto de umwelt acuñado por Jakob Johann von Uexküll.
De igual forma, aporta información bastante relevante con la
aplicación del concepto de semiosfera en relación a lo subjetivo
de un organismo con lo objetivo de la exterioridad que lo rodea.

### La evolución externa de la mente.

Una exterioridad que muchas veces resulta no ser del todo material
en un sentido estricto físico-material, sino que su materialidad
viene del terreno desde donde suceden dichas relaciones entre
lo interno cerebral y lo externo social.

>There are many realities which are neither physical nor mental, but which are beyond and outside the Cartesian antithesis. We cannot enter into a detailed >discussion or definition, but obviously, beside responding to biological needs, human behavior is fundamentally determined by realities which, in a loose way, >we may call cultural, symbolic, spiritual values and the like. It is easy to see that they fall into neither of the Cartesian categories—they are neither >physical, like rocks and animals, atoms and chemical reactions; nor are they mental, like feelings and thoughts, motivations and other psychological >constructs. I suggest that if one is to think this through, he start with trivial facts in our society—say, the Bureau of Internal Revenue as a very real >entity which nevertheless is neither a physical thing nor, unfortunately, a mental hallucination—and go on up to the sublime achievements of culture called >science, works of art, religious values, and so forth. One should think over whether a Beethoven symphony, a Rembrandt painting, or the system of physics can >be defined in terms of the categories of "physical" and "mental." It will easily be found that they cannot be. But it is just such realities on the higher or >symbolic level which determine the most important part of human behavior.@note

Bertalanffy hace un estudio extenso y completamente fuera de
los lineamiento tradicionales de lo mental y de lo físico. Sale
de los cerebral para entrar en el mundo de lo subjetivo a partir
de la inferencia del mundo externo. Nos da muchos elementos y
enfoques interdisciplinares para dar cuenta de ducha relación,
fuera de un enfoque reduccionista físico-materialista pero sin
dejarlo de lado. Resalta la importancia que hay sobre la mente
humana y como es de acuerdo a su evolución social.

De hecho, la mente no podría ser de la manera que es sin haber
evolucionado de la forma en la que lo hizo. Es decir, no somo
seres encerrados en nuestros estados mentales puramente subjetivos
y oscurecidos por un cráneo. La evolución juega un papel tanto
neuronal como social y cultural. Parece que ambos se afectan
mutuamente y no podría haber algo como la conciencia sin la adaptación
de lo mental a lo externo.

>Si bien la mayoría de los acercamientos al problema evolutivo de la conciencia pueden adscribirse a los supuestos metodológicos mencionados, el DN se >caracteriza por llevar el pensamiento poblacional de la teoría de Charles Darwin a la explicación de los fenómenos mentales. Básicamente, la propuesta de >Edelman consiste en la aplicación del darwinismo al nacimiento, evolución y configuración del sistema neural.

>De esta manera, plantea la comparación análoga de la selección en el sistema nervioso central con los procesos selectivos de la evolución darwiniana y el >sistema inmunológico. Si esta analogía es correcta, el sistema cognitivo (particularmente en los homínidos) constituiría un tercero y más reciente desarrollo >que involucraría procesos de selección, cuya principal diferencia entre estos tres es la escala temporal de la operación selectiva: de millones de años para >la selección natural, horas o días para la selección inmunológica y milisegundos para la selección neural.@note

El texto de Kuri y Muñoz Rubio nos da una pista sobre la posibilidad
de la aparición de la conciencia a partir una evolución en término
neuronales en conjunto con lo social. Es decir, la evolución
se aplica y corporal, con todo lo que ello implica. Por consiguiente,
la evolución tendría un papel físico-material en el desarrollo
de una complejidad mayor en el tejido neuronal del cerebro, pero
dicho movimiento iría también de la mano con la adaptación al
medio ambiente y las relaciones que de allí surjan. Una explicación
mental cerebral de manera materialista, sería posible no sólo
con elementos físico sino que con elementos sociales, culturales
y simbólicos.


# Aceleracionismo: ¿diagnóstico o programa?

## Introducción

En este dosier, mi propósito es introducir los aspectos filosóficos
del aceleracionismo, orientado a la siguiente pregunta (que es
también guía de la investigación): ¿Es el aceleracionismo un
diagnóstico o un programa existencial? Considero que dentro de
una realidad enmarcada por el capitalismo, el Aceleracionismo
es una de las expresiones de la subjetividad contemporánea más
importantes, como se expresa en el manifesto Aceleracionista
de Williams y Srnicek:"Si algún sistema ha sido asociado con
la idea del aceleracionismo, es el capitalismo (...) su auto-presentación
ideológica es una de liberar las fuerza de destrucción creativa,
liberando las innovaciones tecnológicas y sociales siempre aceleradas".@note
Podemos afirmar que no es solamente un fenómeno cultural en el
sentido de una tendencia evanescente. Aunque su difusión y desarrollo
posterior a la academia se ha dado en parte de manera análoga
a fenómenos sociales, o subculturas _online_, considero que la
importancia del aceleracionismo se encuentra en su expresión
teórica: un conjunto de problemas de la máxima relevancia para
la filosofía actual. La crisis política, social y subjetiva dentro
del contexto capitalista y tecnológico de los que se ocupa el
pensamiento aceleracionista nos aproximan a la actualidad desde
un horizonte insospechado. Aún más, si en lugar de una práctica
política conservadora en por del desarrollo insaciable y voraz,
partimos de un aceleracionismo en tanto que diagnóstico de la
condición ultracapitalista, se hará posible ponderar sus posibilidades
emancipadoras de esa misma condición que diagnostica. Debido
a que como proyecto teórico, el aceleracionismo ha desarrollado
una autocrítica, acusando los desaciertos y errores de la academia,
y cuya influencia conceptual proviene de filósofos cuyo enfoque
señala las fallas de nuestro sistema de análisis, el aceleracionismo
permite una nueva forma de acercarnos a la crítica.

Con los textos que presento busco lograr una comprensión conceptual
del aceleracionismo, identificar la relación entre su propuesta
y sus formas retóricas propias y por último, exponer la configuración
del aceleracionismo como creador de una teoría que hace frente
a la crisis desde un lugar diferente al que la crítica se ha
reducido, por un lado, y proyecto emancipador por otro.

### Las fuentes del aceleracionismo

#### _El manifesto del partido comunista_ y la subordinación del obrero

En el fragmento seleccionado del _Manifiesto del partido comunista_
se analiza el lugar del obrero frente a lo que Marx y Engels
identifican como los intereses de la burguesía, dominados por
una búsqueda de progreso que otorgue ventaja dentro de la competencia.
Sin embargo, este desarrollo es también un elemento de riesgo
par el salario de los obreros. Esto, aunado con la influencia
de la tecnología en la subordinación del obrero, da lugar al
tipo de relación que el aceleracionismo analiza. En tanto que
el conflicto entre burguesía y obreros surge a partir de deseos
contrarios que no pueden conciliarse, y que están representados
en el dinero, el aceleracionismo postula a la tecnología y al
desarrollo como una forma de producción desbordante de este deseo.@note
Para Marx, la influencia de los objetos y la tecnología sobre
los humanos va más allá de su influencia sobre la brecha salarial;
no se reduce a una relación mercantil ni creadora.

#### “Fragmento sobre las máquinas” y la configuración del sujeto 

En el “Fragmento sobre las máquinas” es clara la manera en la que las máquinas son parte intrínseca de la configuración de los sujetos; son “órganos” que potencializan sus más grandes capacidades y por medio de los cuales se reflejan su racionalidad y cotidianeidad.@note 
 
### Dos lecturas del "Fragmento de las máquinas"

#### Deleuze y Guattari: las máquinas y el deseo

Para Deleuze y Guattari el deseo que identifican Marx y Engels
como contradictorio, es en realidad, la forma más apta del capitalismo,
el resultado más propio. Este deseo resulta entonces no estar
determinado por una ideología, sino por los flujos del propio
mercado. La forma de este deseo es definitorio para la inconcebibilidad
de un futuro disociado del desbordamiento de la lógica de producción
y la razón instrumental nunca completa del capitalismo. En este
fragmento Deleuze y Guattari sugieren la inmanencia del desbordamiento
y exceso de alienación y deseo producidos por el capitalismo;
“acelerar el proceso”.@note La forma de lenguaje presente en
los textos de Deleuze y Guattari, son al igual que su contenido
filosófico y sistemático, una gran influencia para los autores
aceleracionistas.

#### Lyotad: el deseo administrado

La retórica del aceleracionismo sigue la tendencia de la creación
y producción excesiva de sentidos y conceptos claramente presente
en el fragmento de Lyotard. La referencia de un _manager_ como
autoridad que legitima un discurso puede ser entendida también
como la dominación de los elementos capitalistas sobre la palabra
y su interpretación.@note

### La disyuntiva: ¿diagnóstico o programa?

#### Diagnóstico: Benjamin Noys

La riqueza o valor del aceleracionismo se encuentra en un su labor teórica, en ella la crisis actual es renovada con la asistencia de técnicas de construcción conceptual que en sintonía con sus propias postulaciones sobre pasa lo que es actualmente posible o concebible. Benjamin Noys señala otra importancia. Este autor se acerca al aceleracionismo desde la perspectiva desde la que este dosier tiene la intención de aproximarse. Para él es el aceleracionismo no puede ser desdeñado como un mero "vicio de la teoría", sino como “registro” del estado actual de “acumulación”. Sin embargo, es renuente al tipo de conocimiento resultante de su producción excesiva y abstracta.@note Aún más, a partir de la incertidumbre sobre las consecuencias positivas del aceleracionismo como “camino revolucionario” es interesante cuestionar también sus implicaciones potencialmente negativas como consecuencia de su postulación teórica. 
 
#### Programa: _El manifesto xenofeminista_ 
 
Considero que el xenofeminismo puede arrogar claves del valor del aceleracionismo como una alternativa a la crítica. Considero que en este “registro de acumulación” y creación excesiva se pueden llegar a articular formas de emancipación. El xenofeminismo imagina, de manera similar al aceleracionismo el futuro del mundo como un futuro en crisis, de una complejidad excesiva. Ante este panorama apuesta por las mismas tendencias alienantes. Sin embargo, a diferencia de algunas posturas conservadoras del aceleracionismo, el postulamiento ante este panorama es el de un replanteamiento del más alto grado de control; el control del propio deseo que ya no esté orientado a la autodestrucción sino a la construcción colectiva de solidaridad y autodominio. Ante esta postura el aceleracionismo puede obtener una nueva lectura más cercana a su potencial alienante constructivo, más allá de su tendencia acéfala. @note

Montserrat Cruz Herrera {.derecha .espacio-arriba1}

## Fragmentos

“Sin embargo, el desarrollo de la industria no sólo nutre las
filas del proletariado, sino que las aprieta y concentra; sus
fuerzas crecen, y crece también la conciencia de ellas. Y al
paso que la maquinaria va borrando las diferencias y categorías
en el trabajo y reduciendo los salarios casi en todas partes
a un nivel bajísimo y uniforme, van nivelándose también los intereses
y las condiciones de vida dentro del proletariado. La competencia,
cada vez más aguda, desatada entre la burguesía, y las crisis
comerciales que desencadena, hacen cada vez más inseguro el salario
del obrero; los progresos incesantes y cada día más veloces del
maquinismo aumentan gradualmente la inseguridad de su existencia;
las colisiones entre obreros y burgueses aislados van tomando
el carácter, cada vez más señalado, de colisiones entre dos clases.
Los obreros empiezan a coaligarse contra los burgueses, se asocian
y unen para la defensa de sus salarios. Crean organizaciones
permanentes para pertrecharse en previsión de posibles batallas.
De vez en cuando estallan revueltas y sublevaciones.” Karl Marx
y Frederich Engels. _Manifesto del partido comunista_. (Madrid:
Fundación de Investigaciones Marxistas, 2014), 61.

“La naturaleza no construye máquinas, ni locomotoras, ferrocarriles,
telégrafos eléctricos, mulas de hilar, etc. estos son productos
de la industria humana: materia natural es transformada en órganos
de la voluntad humana sobre la naturaleza, o de la participación
humana en la naturaleza. Son órganos del cerebro humano, creado
por la mano humana; el poder del conocimiento, objetificado.
El desarrollo del capitalismo fijado indica en qué grado conocimiento
general social se ha convertido en una fuerza directa de producción,
y en qué grado, entonces, las condiciones del proceso de la vida
social se ha puesto bajo el control del intelecto general y ha
sido transformado en concordancia con ello. En qué grado el poder
de la producción social ha sido producido, no sólo en forma del
conocimiento, pero también como órganos inmediatos de la práctica
social, de los proceso de la vida real.” Karl Marx. “Fragment
on Machines”, en _Accelerate_, editado por Robert Mackay y Armen
Avanessian. (Falmouth: Urbanomic Media Ltd., 2014), 63-4.

“Deseo del asalariado, deseo del capitalista, todo palpita de
un mismo deseo basado en la relación diferencial de los flujos
sin límite exterior asignable y en la que el capitalismo reproduce
sus límites inmanentes a una escala siempre ampliada, siempre
más abarcante. Por tanto, es al nivel de una teoría generalizada
de los flujos que podemos responder a la cuestión: ¿cómo se llega
a desear el poder, la potencia, pero también la propia impotencia?
¿Cómo un campo social semejante pudo ser cargado por el deseo?
¡De qué modo el deseo supera el interés llamado objetivo, cuando
se trata de hacer manar y de cortar flujos! Sin duda, los marxistas
recuerdan que la formación de la moneda como relación específica
en el capitalismo depende del modo de producción que convierte
a la economía en una economía monetaria. Falta que el movimiento
objetivo aparente del capital, que no es en modo alguno un desconocimiento
o una ilusión de la conciencia, muestre que la esencia productiva
del capitalismo no puede funcionar más que bajo esta forma necesariamente
mercantil o monetaria que la domina y cuyos flujos y relaciones
entre flujos contienen el secreto de la catexis de deseo. Es
al nivel de los flujos, y de los flujos monetarios, no al nivel
de la ideología, que se realiza la integración del deseo. Entonces,
¿qué solución hay, qué vía revolucionaria? El psicoanálisis apenas
tiene recursos, en sus relaciones más íntimas con el dinero,
ya que registra guardándose de reconocerlo todo un sistema de
dependencias económico-monetarias en el corazón del deseo de
cada sujeto que trata y que por su cuenta constituye una gigantesca
empresa de absorción de plusvalía. Pero, ¿qué vía revolucionaria,
hay alguna? — ¿Retirarse del mercado mundial, como aconseja Samir
Amin a los países del tercer mundo, en una curiosa renovación
de la «solución económica» fascista? ¿O bien ir en sentido contrario?
Es decir, ¿ir aún más lejos en el movimiento del mercado, de
la descodificación y de la desterritorialización? Pues tal vez
los flujos no están aún bastante desterritorializados, bastante
descodificados, desde el punto de vista de una teoría y una práctica
de los flujos de alto nivel esquizofrénico. No retirarse del
proceso, sino ir más lejos, «acelerar el proceso», como decía
Nietzsche: en verdad, en esta materia todavía no hemos visto
nada.” Gilles Deleuze y Félix Guattari. _El Anti Edipo. Capitalismo
y esquizofrenia_. (Barcelona: Paidós, 1985), 246-7.

“What is this discourse? How is it legitimated? Where is it situated?
Who authorizes you to speak in this way? Are you the manager
[tenancier] of the great skin? But how could you be, when it
is ephemeral and offers nothing to hold onto or secure? Aren't
you concerned with pure imagination and rhetoric? Are you looking
for truth, do you claim to speak it, to have spoken it? Haven't
you produced just another new philosophy, yet another system?
Yet more words? Have these words the pretension to change the
world, at least? If not, what? To interpret it, wretch! In truth
it is pure imaginary fabrication on your part, the realization
of desire on the 'skin of language', as you would put it, aestheticism,
elitism.” “¿Qué es este discurso? ¿Cómo se legitimiza? ¿Dónde
está situado? ¿Quién te autoriza hablar de la manera en la que
lo haces? ¿Eres el manager de la gran piel? ¿Pero, cómo podrías
ser, cuando es efímero y no ofrece nada de qué agarrarse o asegurarse?
¿No te preocupa la pura imaginación y retórica? ¿Estás buscando
la verdad, afirmas pronunciarla, haberla pronunciado ya? ¿No
has producido sólo otra nueva filosofía, aún otro sistema? ¿Aún
más palabras? ¿Tienen estas palabras la pretensión de cambiar
el mundo, por lo menos? ¿Si no, qué? ¡Interpretarla, infeliz!
Es en verdad una fabricación imaginaria de tu parte, la realización
del deseo en la “piel del lenguaje”, como lo dirías, aestecismo,
elitismo.” Jean-Francois Lyotard. _Libidinal Economy_. (Indiana:
Indiana University Press, 1993), 241.

“0x18. XF asserts that adapting our behaviour for an era of Promethean
complexity is a labour requiring patience, but a ferocious patience
at odds with ‘waiting’. Calibrating a political hegemony or insurgent
memeplex not only implies the creation of material infra-structures
to make the values it articulates explicit, but places demands
on us as subjects. How are we to become hosts of this new world?
How do we build a better semiotic parasite—one that arouses the
desires we want to desire, that orchestrates not an autophagic
orgy of indignity or rage, but an emancipatory and egalitarian
community buttressed by new forms of unselfish solidarity and
collective self-mastery?” “0x18. XF afirma que adaptando nuestro
comportamiento para una era de complejidad Prometeana es una
labor que requiere paciencia, pero una paciencia feroz en desacuerdo
con “espera”. Calibrando una hegemonía política o un memeplex
insurgente no sólo implica la creación de infraestructuras materiales
para hacer explícitos los valores que articula, también sitúa
demandas en nosotros como sujetos. ¿Cómo podríamos convertirnos
en anfitriones de este nuevo mundo? ¿Cómo construimos un mejor
parásito semiótico --uno que excite los deseos que queremos desear,
que no orquestre una orgía autofágica de indignidad o ira, pero
sí una comunidad emancipatoria y egalitaria reforzada por nuevas
formas de solidaridad no egoísta y domino propio colectivo? Laboria
Cuboniks. [_Xenofeminism: A politics for alienation._](https://www.laboriacuboniks.net),
9-10.

“It might be easy to dismiss theoretical accelerationism as a
malady of those who take theory too far, spinning-off into abstract
speculation. In fact, the very point of accelerationism is going
too far, and the revelling and enjoyment engendered by this immersion
and excess. They push into the domain of abstraction and speculation
which, with the financial crisis, is evidently the space of our
existence. I am sceptical that such a ‘road of excess’ will,
in William Blake’s words, lead ‘to the palace of wisdom’. It
does, however, lead us to think what this excess and abstraction
might register. If accelerationism is not the revolutionary path
it may be the path that records, in exaggerated and hyperbolic
form, some of the seismic shifts of capitalist accumulation from
the 1970s to the present.” “Podría ser fácil rechazar aceleracionismo
teórico como una enfermedad de aquellos que llevan la teoría
demasiado lejos, escendiendo hacia una especulación abstracta.
En realidad, el punto mismo del aceleracionismo es ir demasiado
lejos, y el deleite y disfrute engendrado por esta inmersión
y exceso. Empujan hacia un dominio de abstracción y especulación
que, con la crisis financiera, es evidentemente el espacio de
nuestra existencia. Soy escéptico de que tal “camino al éxito”
va a, en palabras de William blake, llevarnos a “un lugar de
sabiduría”. Sin embargo, sí nos lleva a pensar que puede registrar
este exceso y abstracción. Si el aceleracionismo no es un camino
revolucionario puede ser el camino que registre, en una forma
exagerada e hiperbólica, algunos de los cambios sísmicos de la
acumulación capitalista de los 70s al presente.” Benjamin Noys.
_Malign Velocities: Accelerationism and Capitalism_. (Alresford:
Zero Books, 2014), 34-5.


## Bibliografía / Referencias:

Deleuze, Gilles y Guattari, Félix. 1985. _El Anti Edipo_. _Capitalismo
y esquizofrenia_. Barcelona: Paidós. {.frances}

Laboria Cuboniks. [_Xenofeminism: A politics for alienation_.](https://www.laboriacuboniks.net)
{.frances}

Lyotard, Jean-Francois. 1993. _Libidinal Economy_. Indiana: Indiana
University Press. {.frances}

Marx, Karl y Engels, Frederich. 2014. _Manifiesto del partido
comunista_. Madrid: Fundación de Investigaciones Marxistas. {.frances}

Marx, Karl. 2014. “Fragment on Machines”, en _Accelerate_, editado
por Robert Mackay y Armen Avanessian. Falmouth: Urbanomic Media
Ltd. {.frances}

Noys, Benjamin. 2014. _Malign Velocities: Accelerationism and
Capitalism_. Alresford: Zero Books. {.frances}

Srnicek, Nick y Williams, Alex. 2013. [_#Accelerate Manifesto._](http://criticallegalthinking.com/2013/05/14/accelerate-manifesto-for-an-accelerationist-politics/)
{.frances}

# Argot político: violencia como manifiesto corporal
 

## Introducción
 

En los últimos años, ha habido casos de violencia contra las mujeres que se posicionaron como referentes para el activismo y pensamiento feminista por su representatividad de las agresiones@note. Los medios de comunicación masivos presentaron los atentados contra sus cuerpos como resultado de una sociedad enferma sexualmente. Si el discurso mediático fuera acertado, quizás esta violencia no sería un problema para la filosofía. Sin embargo, el escenario y las formas de violencia no han sido explicadas exhaustivamente al apelar a libidos desenfrenadas. Sospecho que la filosofía, cuando menos, ha de esforzarse por comprender la lógica de poder subyacente a la violencia ejecutada y ha de indagar en las nociones de subjetividad y enunciación implícitas en las violencias contra los cuerpos femeninos. En el esfuerzo por descifrar la lógica de la violencia contra  los cuerpos femeninos, llegué a Segato. En 1993, Rita Segato asumió el compromiso filosófico —a pesar de no ser filósofa de formación— de dar sentido a la realidad, específicamente, al fenómeno de la violencia contra las mujeres. 
 

Ante el discurso de los medios de comunicación al respecto de la intensificación de la violencia explícita hacia las mujeres — que sostiene que los móviles de estos crímenes son el odio patológico y/o la libido desenfrenada@note—, podemos considerar que el problema de esta violencia tiene un aspecto político que no ha sido tomado en cuenta al atribuirlo a pasiones o condiciones de sujetos aislados. Las investigaciones de Rita Laura Segato (1993) sugieren algunas preguntas filosóficas que guían la literatura consultada: ¿qué valores de organización fundan los sujetos contemporáneos en procesos de violencia?, ¿qué significan las categorías cuerpo, sujeto, violencia y política?, ¿cuál es la función política de la violencia? En el análisis de estos fenómenos, mi hipótesis es que la violencia intensificada hacia las mujeres —como sujetos de categoría política— en el contexto mexicano responde a una estrategia política que pretende conservar el dominio masculino de la esfera pública mediante mensajes en los cuerpos violentados. Mi postura puede explicarse debido a que la representación mediática de tales violencias parece apelar a una restricción de la libertad al colocar como factores de riesgo ocupar espacios públicos, estar activa a ciertas horas e, incluso, los atavíos del cuerpo. Tales aspectos organizan la vida pública de una manera distinta, pues tienen implicaciones en la forma en la que organizamos la relación con el otro/la otra, las normas sociales y las geografías. A continuación, explico las tesis principales de sus obras, pues proporcionan claves necesarias para inteligir la violencia corporal en sentido político. Lo anterior, a partir de la siguiente división tripartita: (1) la violencia corporal hacia las mujeres, (2) la violencia como mecanismo expresivo y de poder, (3) la violencia en su dimensión inscriptiva y enunciativa y (4) representaciones mediáticas en su dimensión de manifiesto político.
 

### La violencia corporal hacia las mujeres


#### Rita Segato: los alcances de la violencia corporal
 

A los crímenes sexuales no son obra de desviados individuales, enfermos mentales o anomalías sociales, sino expresiones de una estructura simbólica profunda que organiza nuestros actos y nuestras fantasías y les confiere inteligibilidad. En otras palabras: el agresor y la colectividad comparten el imaginario de género, hablan el mismo lenguaje, pueden entenderse. […] El trazo por excelencia de la soberanía no es el poder de muerte sobre el subyugado, sino su derrota psicológica y moral, y su transformación en audiencia receptora de la exhibición del poder de muerte discrecional del dominador@note.
 

En _La escritura en el cuerpo de las mujeres asesinadas en Ciudad
Juárez_, Rita Segato señala que el cuerpo de la mujer es el soporte
material mediante el cual el patriarcado@note declara su poder.
Rita Segato rompe con la aproximación tradicional al fenómeno
de las violaciones, la cual suele apuntar como origen del problema
el deseo sexual. A partir de varias cartografías de las expresiones
de la violencia contra las mujeres en América Latina, la antropóloga
atribuye un significado político a la violencia. Considero que
el concepto que puede dar claves de comprensión para aproximarnos
filosóficamente a tan extrema violencia es el poder de muerte
y derrota como mensaje en un cuerpo violentado.


#### ¿Qué dice la violencia?: Violencia corporal como desplazamiento del lenguaje
 

El aporte de esta obra de Segato al argumento del dosier consiste en el desplazamiento del móvil de la violencia, pues sostiene que el valor de la violencia no es por sí mismo, sino que lo crucial es su expresividad. La violencia transmuta a símbolo cuando se encarna porque genera significados en los otros: a unas, nos produce horror tales alcances de la tortura, miedo por ser ella siguiente cuerpo violentado y rabia la injusticia; a unos, contribuye a su reafimación de que el espacio público es suyo y cualquiera que habite en él también lo es, crea alianzas masculinas en la colaboración de los crímenes y su encubrimiento, confirma el dominio de valores misóginos. Lo anterior suscita algunas preguntas: ¿qué diferencia hay en lo que podían decir los cuerpos vivos a lo que dicen los cuerpos asesinados?, ¿a quién se lo dice?, ¿qué les dice un cuerpo violentado a otras mujeres?, ¿qué les dice a otros hombres violentos? ¿Qué cuerpos producen la escritura en otros cuerpos? Estas interrogantes conducen a un estudio especial sobre la composición y los alcances de la violencia en su dimensión expresiva.
 

### Violencia: estrategia expresiva del poder


#### Rita Segato: ¿la violencia tiene una finalidad política?
 

Es por su calidad de violencia expresiva más que instrumental —violencia cuya finalidad es la expresión del control absoluto de una voluntad sobre otra— que la agresión más próxima a la violación es la tortura, física o moral. Expresar que se tiene en las manos la voluntad del otro es el telos o finalidad de la violencia expresiva. […] En un régimen de soberanía, algunos están destinados a la muerte para que en su cuerpo el poder soberano grabe su marca; en este sentido, la muerte de estos elegidos para representar el drama de la dominación es una muerte expresiva, no una muerte utilitaria. […] Si la violación es, como afirmo, un enunciado, se dirige necesariamente a uno o varios interlocutores que se encuentran físicamente en la escena o presentes en el paisaje mental del sujeto de la enunciación@note.
 

En _La guerra contra las mujeres_, Rita Segato advierte que la
violencia contra las mujeres ha mutado en los últimos años. En
las distintas guerras antañas, las mujeres eran consideradas
como motín y territorio: las violaban para incidir en la genealogía
del pueblo, desmoralizar y servir de recompensa a los combatientes.
Ahora, estos cuerpos violentados, sexual y existencialmente,
—en su andar vivo o en su descubrimiento muerto— son la expresión
de la crueldad imperante y de un sujeto autodeclarado superior
en tanto que es capaz de utilizar a este cuerpo otro y explotar
su intimidad, ya no solo de invadir y conquistar, también de
destruir y dominar su existencia. De esta manera, la guerra se
efectúa en la materialidad de los cuerpos femeninos porque contra
ellos está declarada.


#### Los mandatos de la violencia 


Dado lo anterior, y como propongo en la introducción, la violencia puede analizarse en su dimensión de lenguaje, pues comunica el poder de unos sobre otras. Esto permite preguntarse qué nociones de subjetividad y poder están en pugna en cuando un cuerpo es violentado: ¿qué nos disputamos cuando ponemos el cuerpo en la calle?, ¿qué mensaje de poder comunica la violencia contra estos?, ¿esta violencia exige implícitamente un nuevo mandato? De ser así, ¿qué significa? ¿La violencia ejecutada con extrema crueldad nos da indicios para pensar que se trata de una estrategia política de control?, ¿qué alcances de modificación geográfica y subjetiva tiene la violencia?
 

### La violencia en su dimensión inscriptiva y enunciativa


#### Rita Segato: los enunciados de la violencia
 

[En estas guerras] parece estar difundiéndose una convención o código: la afirmación de la capacidad letal de las facciones antagónicas en lo que llamé «la escritura en el cuerpo de las mujeres» (Segato, 2006 y 2013), de forma genérica y por su asociación con la jurisdicción enemiga, como documento eficiente de la efímera victoria sobre la moral del antagonista. Y ¿por qué en las mujeres y por qué por medio de formas sexualizadas de agresión? Porque es en la violencia ejecutada por medios sexuales donde se afirma la destrucción moral del enemigo, cuando no puede ser escenificada mediante la firma pública de un documento formal de rendición. En este contexto, el cuerpo de la mujer es el bastidor o soporte en que se escribe la derrota moral del enemigo@note.
 

En _Las nuevas formas de guerra y el cuerpo de las mujeres_,
Segato advierte que la violencia hacia las mujeres no tiene como
finalidad última atentar contra su materialidad, sino lastimarlas
corpo-emocionalmente de tal forma que resulte incomprensible
el dolor producido. Esta inteligibilidad comunica generar miedo
en las otras la posibilidad real de tal violencia. Así, los cuerpos
violentados funcionan como una declaración de poder al manifestar
la capacidad de unos para dañar a otras. En este sentido, los
cuerpos violentados son anuncios de poder que comunican que un
cuerpo libre fue derrotado, que una voluntad triunfó sobre otra,
que los espacios y las libertades pertenecen ya a algunos. La
violencia corporal intensa intenta comunicar que los cuerpos
femeninos son débiles e ineptos para la hostilidad de los espacios
públicos. Cuando violan ese cuerpo, cuando lo asesinan y lo arrojan,
expresan su control. En estos actos un grupo se reconoce como
superior por su capacidad para deshabilitar la voluntad de otro.
En un esfuerzo por simplificar más, podríamos decir que los ataques
pretenden comunicar lo siguiente: hay un sector de la población
que debe sentirse débil porque sus deseos, su vida tal como la
viven con voluntad, está supeditada a que ciertos grupos decidan
o no intervenir con violencia su existencia arrebatando el poder
que tengan los primeros sobre sí.


#### ¿Por qué resulta necesario emplear la violencia?


El análisis de Segato contribuye a pensar, como sostengo en la introducción, que la violencia es una estrategia política. Esto debido a que se intensifica no por desviaciones, sino porque un grupo está desesperado por conservar su poder y quiere comunicar su dominio en un medio efectivo e inmediato: el cuerpo. Un cuerpo violentado es un enunciado de poder que debe llegar a dos públicos: a aquellas para que se sepan dominadas y a aquellos para que se sepan dominadores. Así, un lenguaje político se funda. Esto suscita diversas cuestiones: ¿por qué atenta tanto un cuerpo femenino libre?, ¿cuáles son sus potencias que han devenido en tanta barbarie?, ¿quiénes hablan sobre y a través de estos cuerpos femeninos violentados? En un esfuerzo por complejizar las preguntas y dar pistas de posibles respuestas es necesario recurrir a materiales mediáticos para interrogar los discursos que comunican.
 
### Representaciones mediáticas en su dimensión de manifiesto político

#### Mariana Berlanga: Los cuerpos como manifiestos
 
Podría tratarse de una mujer trabajadora o una estudiante. La dejaron en la parte posterior de una escuela. ¿Qué estaría haciendo antes de ser capturada por sus agresores? En las articulaciones de las piernas, el pantalón está arrugado. Es como si hubiera estado sentada p hincada durante mucho tiempo. ¿Qué habrá vivido antes de ser ultimada? ¿Habrá sido golpeada, arrastrada, violada? […] Su postura corporal y posición, en relación a los dos hombres, son las de una mujer vulnerable. ¿Cómo se puede ser vulnerable estando muerta? A esta mujer no sólo le arrebataron la vida, sino que la ataron de manos y la dejaron “tirada” en una escuela. ¿Qué hizo? ¿Por qué le amarraron las manos? ¿Qué dijo? ¿Por qué le taparon la boca? ¿Qué representa la exhibición de un cadáver con los brazos amarrados, la boca tapada y la cara cubierta, enterrada en la tierra? Ellos muestran el cadáver en la escuela donde las niñas van a aprender. ¿Qué nos enseña este cuerpo?@note
 
En esta imagen también aparece el cadáver de una mujer en un ángulo inverso, de cabeza a pies, por lo que en primer plano vemos parte del rostro ensangrentado, con un charco de sangre en el suelo. Del cabello despeinado que le cubre la mitad de la cara parece salir un objeto: una especie de arma punzocortante. Parece que le fue clavada en la boca. En este caso, la mujer está vestida: viste suéter negro y pantalones negros con rayas. El suéter deja descubierta parte de la espalda a la altura de la cintura. Es el único pedazo de piel que le podemos ver. Luce mojado, manchado de sangre. Los pantalones son entallados, dejan ver la curvatura de sus glúteos. La posición en la que se encuentra, boca abajo y ligeramente de lado, aunada al ángulo desde donde fue retratada, hace que la silueta de la mujer sea curvilínea. Aquí se repite un elemento observado en la primera fotografía analizada. ¿Por qué las curvas de una mujer son exaltadas aun cuando está muerta?@note
 
En _Una mirada al feminicidio_, Mariana Berlanga, filósofa estudiosa de Segato, analiza la forma en la que los medios comunican la violencia contra las mujeres para desentrañar el discurso que reproducen sobre el poder. De tal labor, el aspecto que más me interesó para los fines de la investigación es la narración de algunas representaciones mediáticas de feminicidios. De ellas explica que funcionan como textos culturales que detentan poder demostrando una capacidad de muerte y de tortura a las mujeres vivas.

#### La pugna entre los cuerpos

Esto abona a la investigación presente para estudiar la lógica
de representación de la pugna entre lo que representan los cuerpos
femeninos en la calle vivos y lo que representan muertos. Esto
permite descifrar una lucha de criterios de organización social
y de modos de vida porque lo inscrito en los cuerpos performa
la política. Lo anterior conduce a cuestionar en qué consiste
la dimensión expresiva del cuerpo: ¿es el cuerpo un espacio de
revuelta en tanto que su materialidad emite declaraciones políticas?
En este sentido, ¿el cuerpo puede volverse un espacio semiótico?
De ser así, ¿hay una forma de pensar la respuesta política de
los cuerpos femeninos ante la rapiña que atravesamos?


## Bibliografía

 
Berlanga, Mariana. _Una mirada al feminicidio_. México: Ítaca / Universidad Autónoma de la Ciudad de México, 2018. {.frances}
 
Segato, Rita. [_La escritura en el cuerpo de las mujeres asesinadas en Ciudad Juárez_](http://www.feministas.org/IMG/pdf/rita_segato_.pdf). Buenos Aires: Editorial Tinta Limón, 2013. {.frances}
 
Segato, Rita. [_Las nuevas formas de guerra y el cuerpo de las mujeres_](https://www.researchgate.net/publication/287830519_Las_nuevas_formas_de_la_guerra_y_el_cuerpo_de_las_mujeres). Puebla: Editorial Pez en el árbol/Tinta Limón, 2014. {.frances}
 
Segato, Rita. [_La guerra contra las mujeres_](https://www.traficantes.net/sites/default/files/pdfs/map45_segato_web.pdf). Madrid: Editorial Traficantes de sueños, 2016. {.frances}

# El sujeto en la colectividad: ¿se pierde, lo agreden o se conserva? 

## El internet como fenómeno autónomo y el sujeto freudiano 

Si en un inicio el internet fue pensado como un fenómeno estrictamente
cultural, ahora las reflexiones en torno a él se ha radicalizado
de modo tal que este comienza a parecer un fenómeno más y más
alejado de la humanidad. De acuerdo con esta idea podemos apelar
a dos nociones : una que entiende el internet como _una_ sola
obra magistral cuyo autor es _indeterminado_ , y otra que (en
palabras del director de cine Werner Herzog) se pregunta por
la posibilidad de que el internet sueñe consigo mismo @note .
Ambas ponen en marcha un espíritu reflexivo que comienza a pensar,
y en el fondo imaginar, lo que el internet podría no sólo existir
como una consecuencia de la actividad humanada en tanto que compuesto
de individualidades o en tanto que foro para la convivencia de
individuales, sino casi como un fenómeno autónomo. Esto supondría
que el internet tiene sus propias condiciones de posibilidad
y sus propios mecanismos, o inclusive que tiene un propio carácter
objetual. Esta nueva noción del internet como entidad propia
a su vez pone en crisis la posición o _situación_ del individuo
que claramente incide en el él pero quizá aquí lo interesante
de nuestro momento actual, ya no lo agote o lo constituya de
modo esencial sino que tan solo primordial.

Es por este motivo que el asunto de un internet autónomo es sobretodo
filosófico y solo superficialmente un problema técnico. Para
ilustrar más claramente el problema del individuo frente al internet,
podemos visualizar el caso del aparente fraude cometido por la
empresa de consultoría política Cambridge Analytica. Esta empresa
lo que logró hacer fue recaudar información acerca de los patrones
de actividad de los usuarios de Facebook y usar algoritmos para
identificar ( ¿ o formular ? ) algo como arquetipos de _personalidades
promedio_. De ahí una pregunta directriz: ¿ esa personalidad
promedio qué tiene que ver conmigo en tanto que individuo, y
aún más si me pienso como sujeto? Es decir, ¿refieren propiamente
a la misma cosa? ¿Tiene uno primacía sobre el otro? ¿Qué sucede
con mi _vivencia_ de ser sujeto? Finalmente, ¿qué puede hacer
el sujeto se rehusa a reducirse a esa personalidad promedio?
En este sentido, la problemática tiene de sí varios tipos de
problemas filosóficos. Por un lado es un problema ontológico
pues nos preocupados por el estatuto de la existencia del sujeto
frente a una entidad digital aparentemente mayor en peso o presencia
(¿ tiene materialidad el internet y si sí, es mayor que la del
sujeto ? ) . Por otro lado es una pregunta epistemológica pues
el internet como una gran obra de arte o como una entidad informativa
pone en tensión nuestras propias maneras de conocer el mundo.
Finalmente, es una cuestión ética; ¿qué hace el sujeto que se
tiene qué definir en esta era?, y ¿ dónde queda la responsabilidad
del sujeto si en el internet es, para efectos prácticos, inexistente
al ser anónimo?

Clarísimo está que esta investigación no puede encargarse de
todas las preguntas hasta ahora expuestas puesto que constituyen
un campo de investigación demasiado amplio. Para limitar la encomienda
a lo posible, se ahondará en la cuestión por la _vivencia_ del
sujeto en el internet bajo el entendido que entre más se aleja
y se independiza el internet del sujeto, más difícil se vuelve
hablar de él. Algo parece suceder-le, suceder-nos. Esta investigación
entonces explorará qué puede ser ese algo, y dirá que se puede
tratar de una perdida, de un ataque o de una conservación. Ninguna
de estas posibilidades es clara ni se explica por sí misma. Para
pensar en una perdida, debemos pensar no en una entidad digital
que pierde al sujeto en tanto que lo suelta, sino que debemos
pensar en el sujeto que ingresa a un espacio y naufraga. Para
pensar en un sujeto atacado, debemos evaluar lo que implica que
una entidad digital ¨ocupe la casa¨ del sujeto, usurpe su subjetividad.
Finalmente, para pensar en un sujeto que se conserva debemos
considerar los distintos modos en los que la subjetividad forma
parte de o constituye aun ese colectivo digital, y luego ponderar
en qué estado se conserva este. El enfoque tampoco puede ser,
por lo menos explícitamente, múltiple. Es preciso de una vez
aclarar si esta investigación pretende tratar la pregunta en
un sentido ontológico, epistemológico o ético. Fácilmente es
posible afirmar que no se pensará desde la epistemología, pues
propiamente la pregunta no es por cómo el sujeto conoce en el
colectivo digital. Empero, es más difícil distinguir entre ontología
y ética. La pregunta, hemos dicho, es por la _vivencia_. A lo
que resuena es por una parte, una pregunta por la _existencia_
y por otra parte, una pregunta por los _modos de vivir_. La exisencia
tiene que ver con la pregunta de por qué estamos en el mundo,
mientras que los modos de vivir tienen que ver con qué podemos
y debemos hacer ya que estamos en este mundo. El matiz parece
sutil pero es importantísimo. Aunque esta investigación considera
que es importante preguntarse por los modos de vivir, ósea por
las posibles maneras de proceder frente al meollo del asunto
(y de hecho hace uso de textos cuyo enfoque sí es ético, en parte
porque son pertinentes, en parte porque le interesan y en parte
porque el vocabulario filosófico hasta ahora es limitado) se
limitará a pensar en la vivencia como una cuestión existencial-
material. La vivencia tal y como se entiende para los propósitos
de la investigación sucede en el mundo, pero no incurre en terrenos
éticos. Así, la vivencia aquí tiene un sentido fenomenológico,
nos preguntamos por cómo es ser un sujeto frente a un colectivo
digital.

## El contexto del problema: el sujeto freudiano en el colectivo 

La organización conceptual de este dosier seguirá el de las categorías
ya expuestas: perdida, agresión y conservación. Pero, antes de
explicar la lo que cada material aporta en relación con ellas
es necesario edificar el terreno conceptual con el cual es posible
pensarlas, si bien luego será necesario ir más allá. Para realizar
esta tarea se invocará el Freud de El malestar en la cultura.
En un inicio, destaca que el limite entre el individuo y su mundo
es siempre difuso y mutable. Esto es clave aquí porque de golpe
evita ciertas trampas naturalistas que clausuran al hombre como
subjetividad terminada o aislada; el punto es siempre empezar
con un pizarrón en blanco y no con supuestos ya dados. Para pensar
la vivencia de la subjetividad en una era digital es necesario
estar dispuesto a ceder terreno. Aquí de nuevo es necesario aclarar
ciertas cosas respecto a lo que se está pensando cuando se dice
vivencia. En Freud, el sujeto es un sujeto del inconsciente lo
cual implica vivencia es siempre, por una parte el resultado
de fuerzas ocultas y por otro lado, siempre sintomática pues
esas fuerzas no se pueden concretar en un plano representacional.
Es entonces apto recordar al lector que esta investigación asume
la presencia del inconsciente. Luego, Freud articula un cierto
tipo de circuito entre el individuo y el colectivo, que él llama
cultura. Para él, el individuo (con su limite inseguro) escupe
su mundo y de ahí genera cultura, pero esta en torno le exige
al individuo la supresión de ciertas pulsiones. Parece ser que
el individuo tiene que modificarse para ingresar dentro de la
colectividad. Es en este contexto que debemos pensar el circuito
sujeto- colectividad digital, pues nos permite (a modo de fundamento)
que la interacción es siempre un movimiento doble. Primero, el
individuo hace su cultura y luego, esta se desemboca sobre su
individuo.

Ya con esta estructura básica a la mano será prudente pensar
específicamente en la vivencia del sujeto dentro de este ciclo.
Igualmente, es necesario buscar otro tipo de material más históricamente
pertinente pues no es menor que Freud no haya conocido el internet
y por lo tanto concibe la cultura sobre todo como sociedad y
como producción cultural. Su noción de colectividad no es lo
suficientemente extensa como para pensar el internet como una
entidad a parte de la cultura. Esta siguiente horda de material,
se ha de prevenir, es de origen diverso y por lo general tiene
siempre un tinte más ético en el sentido de modos de vivir. Esta
investigación en cierto sentido tiene que acoplarse al tipo de
textos disponibles , y también tiene como encomienda preguntarse
por qué no hay reflexiones filosóficas ontológicas respecto del
tema.

### El sujeto se pierde en una colectividad: 

Uno de los materiales que esta investigación usará para pensar
en la perdida es el caso ya anteriormente mencionado de Cambridge
Analytica. Este caso pone en relieve la posibilidad de generar
universalidades que parecen reflejar y abarcar a las subjetividades
individuales. Es interesante e importante para esta ejercicio
investigativo pues lo que señala, se infiera a partir de las
reacciones que generó, que el individuo no se reconoce en estas
personalidades promedio. Es decir, no me hallo en ella. La personalidad
promedio no es capaz de dislumbrar lo que me hace mi , ¿o sí?
El siguiente material que se usara para pensar un texto de la
esteta Hito Streyerl, y más bien funge como un manual o sugerencia
de cómo encontrarse en el colectivo digital. Es interesante pues
propone re pensar o re definir los supuestos fundamentales del
sujeto, de modo tal que este tome un paso, o más bien retorne,
a lo material desde lo representacional. Dice: "Llegados a este
punto, ¿qué sucede con la identificación? ¿Con quién nos podemos
identificar? Evidentemente, la identificación se produce siempre
con respecto a una imagen. Pero pregúntenle a cualquiera si de veras le gustaría
ser un archivo JPEG. Y aquí reside precisamente mi argumento:
si la identificación bien tiene que ver con algo es con este
aspecto material de la imagen, con la imagen como cosa, no como
representación. Y entonces quizá deja de ser identificación y
en su lugar se convierte participación" @note . Aquí se entiende
que el sujeto tiene que renunciar a su dimensión representaciones
si pretende ser _visible_ dentro del colectivo digital. El modo
de escapar la sujetación del sujeto es, paradojicamente, volverse
objeto. Esto porque la materialidad es antídoto del trauma freudiano,
que a su vez implica un desplazamiento representacional infinito
hacia adentro del sujeto, y es sujeto de ser (como Cambridge
Analytica) representado digitalmente. El trauma freudiano es
el la dimension _explotable_ del sujeto.

### Al sujeto lo vence una colectividad: 

Para pensar en la agresión hacia el sujeto por parte del colectivo
digital es un manifiesto escrito por Jaron Lanier, un filósofo
de a computación y uno de los fundadores de la realidad virtual.
En la sección You Need Culture to Even Percieve Information Technology,
señala que el colectivo digital hace parecer como si el internet
estuviera más vivo que los humanos. En este sentido, diluye o
debilita la vivacidad del humano. Similarmente a Hito, comprende
a la cultura como materialidad de tal modo que cree que un bullet
verdadero te mata mientras que un bullet virtual solo mata tu
representación digital. Luego, la cyberfeminista Remedios Zafras
aporta algo a la idea de agresión en su libro Un cuarto propio
conectado. Ahí, se dedica a sensibilizar al lector hacia las
implicaciones que tiene una computadora, ahora el objeto primario
del cuarto privado que pensó Wolf en algún momento. El mundo
digital es portable, por lo cual se instala los espacios íntimos
del sujeto, los espacios en los cuales el sujeto tradicionalmente
se gesta y se resguarda.

### El sujeto está ( o puede estar ) intacto dentro de una colectividad:

Para pensar en la idea de conservación del sujeto dentro del
colectivo digital es interesante pensar en formatos de edición
colaborativa como lo es Wikipedia. En wikipedia los colaboradores
no importan en tanto que su nombre, pero cualquier sujeto puede
ver el resultado material de su aportación. Es un ejemplo con
positivo de lo que puede implicar la conservación del sujeto.
Finalmente, de nuevo en El malestar de cultura Freud nos dice
cosas interesantes. Usa la zoología para exponer que del mismo
modo que inclusive en las estructuras taxonómicas complejas es
posible identificar formas de vida primeras, es posible identificar
al individuo dentro del colectivo. Asimismo, cuestiona de qué
modo permanece esa pues introduce la posibilidad de que algo
desde siempre ya dañado perdure.

#### Fragmentos utilizados: 

1. “Advertisers and marketers use several methods to derive psychometric
profiles: the easiest and most direct option is to conduct a
survey that reveals aspects of the participants' psychological
composition. The survey data is then analysed to create a psychometric
profile of the individual or group. More recently, researchers
have discovered that conducting a mass survey to infer these
traits is unnecessary. Instead, these traits can simply be predicted
from alternative sources like Facebook data... Cambridge Analytica
identified and targeted persuadable voters in the run-up to the
2016 US Presidential election, first for Ted Cruz, and then for
Donald Trump's campaign...By harvesting psychological survey
data, constructing algorithms to predict psychological traits,
and extrapolating these results more widely, the company "was
able to produce a model of the personality of every single person
in the United States of America." This, in turn, allowed Cambridge
Analytica to deliver micro-targeted ads to potential voters based
on their profiles on hot-button issues like their feelings toward
the Second Amendment.” @note
2. “Llegados a este punto, ¿qué sucede con la identificación? ¿Con quién
nos podemos identificar? Evidentemente, la identificación se produce
siempre con respecto a una imagen. Pero pregúntenle a cualquiera si
de veras le gustaría ser un archivo JPEG. Y aquí reside precisamente
mi argumento: si la identificación bien tiene que ver con algo
es con este aspecto material de la imagen, con la imagen como
cosa, no como representación. Y entonces quizá deja de ser identificación
y en su lugar se convierte participación... Tradicionalmente,
la práctica emancipatoria ha estado ligada a un deseo de convertirse
sujeto. La emancipación se concebía como devenir sujeto de la
historia, de la representación o de la política. Devenir sujeto
conllevaba la promesa de autonomía, soberanía y acción. Ser un
sujeto era bueno ; ser objeto era malo. Pero, como todo el mundo
sabe, ser un sujeto puede tener sus complicaciones. El sujeto
está siempre ya sujeto. Si bien la posición de sujeto implica
un cierto grado de control, en realidad está sujeta a relaciones
de poder...¿Y si la verdad no se encuentra ni en lo representado
ni en la representación? ¿Y si la verdad se encuentra en la configuración
material de la imagen? ¿Y si el medio es en realidad un masaje? ¿O más
bien -en su versión mediática corporativa- una lluvia de intensidades
mercantilizadas? Participar en una imagen -en lugar de sencillamente
identificarse con ella- quizá podría abolir esta relación. Significaría
participar en la materialidad de la imagen tanto como en los
deseos y fuerzas que esta acumula...Este cambio de perspectiva
conlleva consecuencias importantes. Quizá siga habiendo un trauma
interno e inaccesible que constituye la subjetividad. Pero el
trauma es también el opio contemporáneo de las masas, una propiedad
en apariencia privada que simultáneamente invita y se resiste
a su embargo. Y la economía de este trauma constituye los restos
del sujeto independiente”. @note
3. “Ever more extreme claims are routinely promoted in the new
digital climate. Bits are presented as if they were alive, while
humans are transient fragments. Real people must have left all
those anonymous comments on blogs and video clips, but who knows
where they are now, or if they are dead? The digital hive is
growing at the expense of individuality. Kevin Kelly says that
we don‟t need authors anymore, that all the ideas of the world,
all the fragments that used to be assembled into coherent books
by identifiable authors, can be combined into one single, global
book. Wired editor Chris Anderson proposes that science should
no longer seek theories that scientists can understand, because
the digital cloud will understand them better anyway. * Antihuman
rhetoric is fascinating in the same way that self-destruction
is fascinating: it offends us, but we cannot look away. The antihuman
approach to computation is one of the most baseless ideas in
human history. A computer isn‟t even there unless a person experiences
it. There will be a warm mass of patterned silicon with electricity
coursing through it, but the bits don‟t mean anything without
a cultured person to interpret them. This is not solipsism. You
can believe that your mind makes up the world, but a bullet will
still kill you. A virtual bullet, however, doesn´t even exist
unless there is a person to recognize it as a representation
of a bullet. Guns are real in a way that computers are not.”
@note
4. “Hoy conviven viejos y nuevos modelos de organización espacial
y política de nuestros tiempos y lugares propios, donde la implicación
personal y crítica resultaría más necesaria que nunca. Pasa
además que acontece una transformación determinante en la esfera
privada y doméstica: la Red se instala en mi casa. [...] La
Red vincula el espacio privado de muchas maneras diferentes con
el mundo exterior y la esfera pública, [...] y en este entramado
[...] ocurren oportunidades de acción colectiva y social limitadas
antes al «afuera del umbral». Lo privado se funde literalmente
con lo público, y entonces lo político se incrementa, [...]
porque esa combinación entre cuarto propio, soledad, anonimato
e intersección público-privada... tiene potencia subversiva”.
@note
5. “Wikipedia is written collaboratively by largely anonymous
volunteers who write without pay. Anyone with Internet access
can write and make changes to Wikipedia articles, except in limited
cases where editing is restricted to prevent disruption or vandalism.
Users can contribute anonymously, under a pseudonym, or, if they
choose to, with their real identity. The fundamental principles
by which Wikipedia operates are the five pillars. The Wikipedia
community has developed many policies and guidelines to improve
the encyclopedia; however, it is not a formal requirement to
be familiar with them before contributing.” @note
6. “Así, en lo que refiere a la serie zoológica, sustentamos la
hipótesis de que las especies mas revolucionadas han surgido
de las inferiores, pero aun hoy hallamos, entre las vivientes,
todas las formas simples de la vida…Tocamos aquí el problema
general de la conservación en lo psíquico, problema apenas hasta
ahora elaborado, peor tan seductor e importante que podemos concederle
nuestra exención por un momento, pese a que la oportunidad no
parezca muy justificada. Habiendo superado la concepción errónea
de que el olvido, tan corriente paa nosotros, significa la destrucción
o aniquilación del resto mnemónico, nos inclinamos a la concepción
contraria d que en la vida psíquica nada de lo una vez formado
puede desaparecer jamas; todo se conserva de alguna manera y
puede volver a surgir en circunstancias favorables, como por
ejemplo, una regresión a suficiente profundidad.” @note
	
#### Bibliografía: 

Bashyakarla, Varoon. [_Psychometric Profiling: Persuasion by
Personality in Elections, Our Data Our Selves_]. (https://ourdataourselves.tacticaltech.org/posts/psychometric-profiling/)
{.frances}

Freud, Sigmund. 1966._El malestar en la cultura_. Madrid: Alianza
Editorial.{.frances}

_Lo and Behold, Revereies of the Connected World_. Directed by
Werner Herzog. Magnolia Pictures, 2016.

Lanier, Jaron. 2010._You Need Culture to Even Percieve Information
Technology, You Are Not a Gadget: A Manifesto_. New York City:
Alfred A Knopf.

Streyerl, Hito. 2014._ Los condenados de la pantalla_. Buenos
Aires: Caja Negra Editora.

_Wikipedia: About, Wikipedia_. https://en.wikipedia.org/wiki/Wikipedia:About

Zafra, Remedios. 2010._Un cuarto propio conectado_. (Ciber)espacio
y (auto)gestión del yo_. Madrid: Fórcola Ediciones. Ximena Gabilondo


Dosier
Introducción:

#Título

La pregunta latente que enmarca los materiales de este dosier es, si comprender el vegetarianismo como una opción o preferencia individual
independiente de la filosofía no reproduce las jerarquías especistas que la filosofía crítica tendría que interrogar e incluso combatir hoy en
día. Esta misma pregunta es también el hilo conductor del trabajo final a presentar, el cual no puede lograrse sin el apoyo en los textos,
fragmentos, y autores seleccionados en este dosier, y algunos otros que se desprenden de los mismos. 
 
No podemos hablar de feminismo en singular, sino de feminismos. Dentro de estos feminismos hay distintas posturas, ramas, teorías y más, y es en
esas líneas de donde surge el eco feminismo. Igualmente, el ecofeminismo suele tener una definición general: según el diccionario de Oxford se
trata de un movimiento que trata la dominación del patriarcado sobre la naturaleza como algo indisociable de la dominación hacia las mujeres. Esta
definición abre el diálogo para entender la intrincada relación entre las distintas formas de discriminación, las cuales no deben excluir la
discriminación y dominación de la naturaleza, llámese mundo animal o vegetal, y que son también estas prácticas producto del sistema patriarcal. 

Si bien esta definición es el punto de partida, sigue siendo
una muy general. El eco feminismo también se divide y existen
diversas posturas. Una de estas perspectivas es la que encuentra
en las mujeres una especie de conexión con la naturaleza, como
si el cuidado que dan las mujeres se extendiera también a la
naturaleza. Bajo esta perspectiva, son las mujeres una especie
de cuidadoras de los animales y las plantas, haciendo énfasis
a las teorías que ---- a las mujeres como quienes cuidaban de
los sembradíos, quienes recolectaban las verduras y actividades
afín.

Hay otra perspectiva, la cual este dosier y trabajo retoma: no
se trata de si son las mujeres o los hombres los que deben cuidar
al mundo vegetal y animal, sino que es el sistema patriarcal
el que ha perpetuado las formas de discriminación presentes en
nuestra sociedad, racismo, sexismo, xenofobia, especismo y más
parten de la misma raíz. Esta raíz es la estructura de dominación
patriarcal, la cual toma, consume, y desecha los “objetos” a
su paso, objetos entre comillas pues estos pueden ser también
humanos, animales no humanos, y el mundo vegetal. Paradójicamente,
esta destrucción del Otro es lo que ha nos ha llevado a la crisis
ambiental actual, de la cual nadie tiene escapatoria.

Aunque en los debates feministas actuales, específicamente las
líneas feministas y eco feministas más ad hoc a la postura a
presentar en este trabajo y dosier, se reconoce que la liberación
de las mujeres es indisociable de la liberación de otras formas
de opresión; sin embargo, en la política feminista predominante
de hoy en día, se continúa reproduciendo el sesgo antropocéntrico
de la política occidental al enfatizar el sujeto ‘mujeres’ en
lugar de igualdad entre especies.

Para exponer lo anterior, recurro al término de ‘vegetarianismo’
el cual recientemente ha mutado a ‘veganismo’, pues el vegetarianismo
suele quedarse corto. Este concepto lo defino tras el libro de
Carol J. Adams “La política sexual de la carne” en el cual establece
que el vegetarianismo es una decisión ética que elimina el consumo
de carne de la dieta, productos y entretenimiento pues considera
el hacerlo una explotación injustificable de los animales. El
vegetarianismo asocia el consumo de carne con los valores patriarcales.

El vegetarianismo se ha quedado corto, pues si bien empezó como
una postura que obviaba el no consumir ningún tipo de producto
animal, el hacer énfasis en la carne generó que en los últimos
años se formaran dos puntos, sobre todo en el ámbito alimenticio:
vegetarianos y veganos. Los vegetarianos entonces eliminan cualquier
consumo de carne animal, pero pueden incorporar el uso de productos
lácteos y huevos y suelen solo enfocarse en su consumo de alimentos,
adoptando una dieta “basada en plantas”; los veganos eliminan
cualquier tipo de opresión y sufrimiento animal, que permea el
no comer ningún tipo de producto que provenga de un animal, así
como tampoco promover su explotación al comprar, consumir o visitar
lugares, productos, empresas que los lastimen o utilicen de alguna
manera.

El veganismo, al ser una postura ética y filosófica, está ligada
al activismo. De aquí surge el siguiente concepto: el especismo
(o especiesimo en algunas traducciones). El veganismo es antiespecista.
Para hablar del especismo retomo un fragmento del artículo de
Sandra Baquedano Jer “Jerarquías especistas en el pensamiento
occidental” la cual traza el primer uso de este concepto hacia
1971 en una denuncia de los experimentos que se le hacían a los
animales en los laboratorios. Tal como el racismo proviene de
raza, el sexismo de sexo, y más, el especismo proviene de especie.
Alude a un tipo de discriminación que el humano ejerce sobre
otros seres vivos, otras especies no humanas. Esta discriminación
y dominación está sustentada en distintos pensamientos que han
normalizado en situar al ser humano como un ser superior ante
las demás especies, y que éstas otras están a su disposición.

A fin de esclarecer y evidenciar las dimensiones filosóficas
del activismo vegetariano resulta útil sumergirnos en distintos
autores que tratan el tema de especismo y vegetarianismo / veganismo.
De ahora en adelante, cuando hable de ‘vegetarianismo’ me estoy
refiriendo al vegetarianismo inicial, el cual elimina cualquier
tipo de producto animal de la dieta y cualquier otro tipo de
consumo. Es decir, a lo que ahora coloquialmente se le llama
‘veganismo’. En primer lugar aparece Peter Singer con su obra
de 1975 “liberación animal”. Si bien no fue elprimer pensador
en tratar el tema, si es el primero en generar suficiente diálogo
y polémica para que entrara en las discusiones filosóficas del
momento y hasta la actualidad.

Val Plumwood

Sandra Baquedano


Fragmentos seleccionados

Definir los términos y conceptos a tratar

Eco feminismo A movement in which mankind's unfortunate domination
over nature is seen as analogous to mens' equally unfortunate
domination over women—an alliance that adds resonance both to
ecological and feminist concerns.


Vegetarianismo “[The] self-conscious omission of meat because
of ethical vegetarianism, that is, vegetarianism arising from
an ethical decision that regards meat eating as an unjustifiable
exploitation of the other animals. [...] The vegetarian quest
consists of: the revelation of the nothingness of meat, naming
the relationships one sees with animals, and finally, rebuking
a meat eating and patri- archal world.


Especismo / especiecismo
Aunque este problema tenga un origen prehistórico, el concepto speciesism surgió solo en 1971 y se lo debemos a Richard Ryder cuando acuñó el
término en su artículo Experiments on Animals en vista de denunciar los crueles experimentos que se realizaban en los laboratorios con animales.
(p. 79). Ahí señala que especismo proviene de especie, así como racismo de raza. Este último se produce a nivel de intraespecie, mientras que el
primero supone el traspaso de ella. No se trata de una igualdad o semejanza sus-tancial entre ambos fenómenos sino de un análogo referencial,
pero en uno y otro caso la comparación alude a una discriminación. En lo que concierne al especismo, aquella que ejerce el ser humano contra un
sinnúmero de seres vivos no humanos, basada precisamente en la pertenencia a una especie. Esta forma de discriminación se aplica en general a
través de la creencia que afirma la superioridad de una especie en detrimento de las demás y preconiza, entre otras cosas, la separación de
especies o grupos por segregación en condiciones de vulnerabilidad.  


Perspectivas

Peter Singer “Liberación animal”

"Entre los factores que dificultan la tarea de provocar el interés
público por los animales destaca, como uno de primer orden, el
supuesto de que «los humanos están primero» y que cualquier problema
relativo a los animales no puede ser comparable a los problemas
de los hombres, tanto por su seriedad moral como por la importancia
política del tema. Sobre este supuesto debe hacerse una serie
de puntualizaciones. Primero, es en sí mismo una muestra de especismo.
¿Cómo puede saber nadie, sin antes haber hecho un estudio detallado,
que el problema es menos serio que los problemas relativos al
sufrimiento humano? Únicamente se estaría en posición de defender
esta tesis si se asume que los animales realmente no importan
y que, aunque sufran mucho, su sufrimiento es menos importante
que el de los humanos. Pero el dolor es el dolor, y la importancia
de evitar el dolor y el sufrimiento innecesarios no disminuye
porque el ser afectado no sea un miembro de nuestra especie.
¿Qué pensaríamos si alguien nos dijera que «los blancos están
primero» y que, por tanto, la pobreza en África no plantea un
problema tan serio como la pobreza en Europa?"


Val Plumwood “Feminism and the mastery of nature” 

“Women have faced an unacceptable choice within patriarchy with
respect to their ancient identity as nature. They either accept
it (naturalism) or reject it (and endorse the dominant mastery
model). Attention to the dualistic problematic shows a way of
resolving this dilemma. Women must be treated as just as fully
human and as fully part of human culture as men. But both men
and women must challenge the dualised conception of human identity
and develop an alternative culture which fully recognises human
identity as continuous with, not alien from, nature. The dualised
conception of nature as inert, passive and mechanistic would
also be challenged as part of this development.”

Sandra Baquedano “Jerarquías especistas en el pensamiento occidental”
La situación contemporánea no es solo de destrucción del entorno,
sino de jerarquías especistas que nacen en el interior del ser
humano y que externamente dejan también al descubierto el complejo
de la autodestrucción, ya que el daño a la naturaleza, al resto
de las formas de vida, puede ser considerado una forma de autodestrucción
en cuanto involucra al individuo y por exten- sión también
a la especie, tratándose de una destrucción activa del entorno
natural necesario tanto para la vida del ser humano como para
la preservación de la biodiversidad.

Debates Angélica Velasco Sesma “La ética animal: una cuestión
feminista?”


## Bibliografía / Referencias

# ¿Acaso siempre percibimos la maternidad como un obstáculo?

Aunque la maternidad en la tradición del feminismo que tiene
a Simone de Beauvoir en el centro considera que la maternidad
es ante todo una desventaja y un obstáculo para la realización
de la mujer. En este dosier se pretende sustentar que es posible
y deseable argumentar que la maternidad en algunos contextos
no constituye necesariamente un obstáculo, sino que por el contrario,
conforma un terreno de emancipación en más de un sentido.

## I. La maternidad en tanto que obstáculo y no-obstáculo

Si bien, la maternidad en la tradición feminista (en la que Simone
de Beauvoir está en el centro) es considerada, ante todo, como
una desventaja y un obstáculo para la realización de la mujer.
En este dosier, se intentará sostener que es posible y deseable
argumentar que la maternidad, en otros contextos, no constituye
necesariamente un impedimento para el desarrollo de la mujer
como sujeto. Sino, por el contrario, la maternidad incluso podría
constituir un terreno de emancipación en más de un sentido. Por
consiguiente, para sustentar lo anterior, se incluirán una selección
de seis textos que se orientan hacia la exploración de la maternidad
latinoamericana bajo la óptica filosófica y feminista. Además,
se pretende exhortar al lector a ir más allá de nociones europeas
adaptadas para Latinoamérica que explican las situaciones propias
de una región bajo conceptos importados.

### I.I Una interpretación de Beauvoir con respecto de las nuevas maternidades. 

Para comenzar, se introducirá un pequeño fragmento, del artículo
“A Process without a Subject: Simone de Beauvoir and Julia Kristeva
on Maternity” de Linda M.G. Zerilli. En este artículo, Zerilli
compara las visiones de Beauvoir y de Kristeva sobre la maternidad
asistida y otros aspectos de la crianza actual. Sin embargo,
para este dosier, con la selección de este artículo se intenta
resaltar cómo la maternidad, desde la perspectiva de Beauvoir,
impide que las madres se afirmen como sujetos porque sus hijos
tienden a reducirlas a meros contenedores de la especie. El siguiente
fragmento muestra la lectura de Zerilli sobre la proposiciones
de Beauvoir sobre la maternidad. En él, se observa que Beauvoir,
según Zerilli, insiste en que las mujeres para continuar con
su propia voz (female speech), deben separar simbólicamente su
cuerpo del cuerpo maternal porque, de otra forma, se subordinarán
a sus hijos.

#### Zerilli: “A Process without a Subject: Simone de Beauvoir and Julia Kristeva on Maternity ”

Beauvoir, for her part, tells us that we ought not affirm that
mothers are masters of a process that does indeed throw into
question the very possibility of mastery. Yet she also tells
us that we need to insist on female speech and on marking a symbolic
border within the maternal body if women are to resist being
made into the passive bearers of a species teleology. Beauvoir's
rhetorical war on the eternal maternal, then, does not simply
instruct feminists to reject maternity; rather, it offers us
a complex strategy for challenging, in Kaja Silverman's words,
"dominance from within representation and meaning rather than
from the place of a mutely resistant biology" (1988, 125). And
in a world of "fetal person- hood" and "surrogate motherhood,"
I would conclude, this is reason enough to reread and rethink
the controversial account of motherhood in The Second Sex. @note

### I.II ¿Latinoamérica percibe la maternidad diferente?

Ahora, para abrir el debate sobre si es posible percibir la maternidad como lo opuesto a un obstáculo, este dosier presentará el artículo: “La maternidad como proyecto individual y autónomo. El caso de las madres solas por elección” de Giallorenzi. Ese texto es una análisis feminista sobre un grupo de mujeres universitarias argentinas que, por decisión, son madres solteras. En las declaraciones de estas mujeres se presentan los motivos que llevaron a las mujeres argentinas a querer hijos sin estar insertas en una relación de pareja. Con la selección de este texto se pretende señalar que, aun, cuando la maternidad de estas mujeres fue revisada bajo los conceptos feministas de Badinter, de Rich, de Friedan, de Beauvoir y de Federici, la perspectiva de ellas, sobre su maternidad, no termina por empatarse con las teorías de las pensadores.
 
#### Giallorenzi: “La maternidad como proyecto individual y autónomo. El caso de las madres solas por elección”

Las familias lideradas por mujeres que deciden y planifican una
maternidad en solitario son una realidad social y cultural que
emerge desde hace un tiempo en nuestras sociedades occidentales.
Como ya se mencionó, la novedad del caso no reside en que estas
mujeres lleven a cabo su maternidad en solitario sino en la planificación
de la maternidad como un proyecto autónomo e independiente de
la conyugalidad. […]

Por un lado, se puede ver como en el relato aparecería una coincidencia
entre el deseo de ser madre y el periodo fértil femenino. Esta
asociación, que se plantea como algo individual promovido por
el deseo interior, es pensada desde las teorías feministas como
el resultado de una construcción social y cultural del sistema
patriarcal que, a partir de múltiples dispositivos educativos
y culturales, le ha inculcado a la mujer la obligatoriedad de
ser madre para afirmar su condición de mujer (Rich, 1986; Beauvoir,
2012; Federici, 2013). […]

En ambos casos [la fertilización en vitro y la adopción] la elección
y la intención de ser madres dan origen a la relación de filiación,
donde la crianza y el vínculo afectivo constituyen elementos
suficientes para construir relaciones de parentesco. Es la capacidad
de agencia de las mujeres, el trabajo de su imaginación sobre
cómo hacer las conexiones, como crear las relaciones, como definir
la maternidad, lo que reconfigura la identidad e integra al hijo/a
en el seno de las relaciones de parentesco, sin desestabilizarlas
(Fitó, 2013). En relación con la crianza y al cuidado de los
hijos/as, las entrevistadas destacan como positivo el hecho de
no compartir las decisiones sobre la crianza con una pareja.
@note

## II. Para pensar la maternidad ¿es necesario importar conceptos?	

### II.I Importar conceptos

Los siguientes artículos del dosier (“¿Puede hablar el subalterno?”
de Spivak y “Las mujeres del «tercer mundo» y el pensamiento
feminista occidental” de Morton), se escogieron para preguntar
si los conceptos desarrollados en Europa o en Estados Unidos,
al aplicarse en otras regiones con diferentes contextos, tienen
la misma fuerza explicativa con respecto del medio en el que
se generaron. El primer texto, “¿Puede hablar el subalterno?”
de Gayatri Spivak, cuestiona qué tan partícipe es la voz del
subalterno (el otro-excluido) del discurso hegemónico y discute
sobre el papel del intelectual dentro de la conformación de ese
saber predominante. En ese sentido, ella expresa que en la aseveración
del intelectual sobre una situación que “de hecho” es como la
él la describe, cierra la epistéme alrededor de su experiencia
concreta. Por su parte, “Las mujeres del «tercer mundo» y el
pensamiento feminista occidental”, puntualiza sobre cómo Spivak
marcó algunos "puntos ciegos" de la teoría feminista occidental.
Porque, según Morton, al Spivak proponer el esencialismo estratégico
se está poniendo en duda la efectividad de las categorías esencialistas
de identidad humana para explicar todas la situaciones que acontecen
en todo el globo.

#### Spivak: “¿Puede hablar el subalterno?”

Foucault articula otro corolario de la negación del rol de la
ideología en la reproducción de las relaciones sociales de pro-
ducción: una valoración incuestionable del oprimido como su-
jeto, el “ser objeto”, como Deleuze subraya admirablemente, “establecer
condiciones donde los prisioneros serían capaces de hablar por
sí mismos”. Foucault añade que “las masas saben perfectamente
bien, claramente” –una vez más la temática del ser desengañado–
“saben mejor que [el intelectual] y ciertamente lo dicen muy
bien” (FD: 206, 207).

¿Qué le sucede a la crítica del sujeto soberano en estos pro-
nunciamientos? Llegamos a los límites de este realismo repre-
sentacionalista con Deleuze: “La realidad es lo que de verdad
sucede en una fábrica, en una escuela, en las barracas, en una
prisión, en una estación de policía” (FD: 212). Esta exclusión
de la necesidad de la difícil tarea de hacer producción ideológica
contrahegemónica no ha sido saludable. Ha ayudado al empirismo
positivista –el principio justificante del neocolonialismo capitalista
avanzado– a definir su propia arena como “experiencia concreta”,
“lo que ocurre realmente”. Por supuesto, la expe- riencia concreta
que es la garante de la apelación política de prisioneros, soldados
y escolares es revelada por medio de la experiencia concreta
del intelectual, que diagnostica la episteme. Ni Deleuze ni Foucault
parecen conscien- tes de que el intelectual dentro del capital
socializado, esgrimiendo la experiencia concreta, pueda ayudar
a consolidar la división internacional del trabajo. @note

#### Morton: “Las mujeres del «tercer mundo» y el pensamiento feminista occidental”

Contra este sistema binario de pensamiento, Spivak propone una
estrategia crítica, que remeda la representación negativa de
los grupos minoritarios tales como las mujeres, los subalternos
o las clases trabajadoras. Spivak se refiere a esta estrategia
crítica como esencialismo estratégico.

__Esencialismo estratégico__

La idea del esencialismo estratégico acepta que las categorías
esencialistas de identidad humana deben ser criticadas, pero
enfatiza que no se puede evitar usar tales categorías en ocasiones
a fin de dar sentido al mundo social y político. En sus primeras
contribuciones a la teoría feminista y postcolonial durante los
80, Spivak propuso un «uso estratégico>>. […] El esencialismo
estratégico por lo tanto es más efectivo como una estrategia
específica para un contexto, pero no puede proveer una solución
política a largo plazo para acabar con la opresión y la explotación.
[…] Sin embargo Spivak ha cambiado el foco del debate esencialista,
de una preocupación con la diferencia sexual entre hombres y
mujeres a enfocar las diferencias culturales entre mujeres del
«tercer mundo» y del «primer mundo». @note

###II.II Sostener análisis de conceptos importados

El cuarto documento del dosier, _Existe el amor maternal_ de Badinter, recuenta cómo históricamente se ha entendido la maternidad en Europa y en Estados Unidos y concluye que maternidad nunca ha sido un concepto universal ni estático. En este recuento de la maternidad a través de la Historia, se aclara que su noción actual comenzó a desarrollarse en 1780 y que ha sufrido modificaciones enormes qué se entiende por amor maternal. Por ello, el fragmento de este material que fue seleccionado para este dosier, es empleado como una exhortación a continuar generando nociones sobre la maternidad propias para los múltiples contextos. Pues, en él se aprecia a la autora rectificando que la maternidad nunca se ha vivido de la misma manera ni se ha tenido una misa noción compartida por diferentes poblaciones.
 
#### Badinter: _Existe el amor maternal_ 

Al recorrer la historia de las actitudes maternales, nace la
con- vicción de que el instinto maternal es un mito. No hemos
encontra- do ninguna conducta universal y necesaria de la madre.
Por el contrario, hemos comprobado el carácter sumamente variable
de sus sentimientos, de acuerdo con su cultura, sus ambiciones,
sus frustraciones. Cómo no llegar a partir de allí a la conclusión
de que el amor maternal es sólo un sentimiento, y como tal esencialmente
contingente, aunque sea una conclusión cruel. Este sentimiento
puede existir o no existir; puede darse y desaparecer. Poner
en evidencia su fuerza o su fragilidad. Privilegiar a un hijo
o darse a todos Todo depende de la madre, de su historia y de
la Historia. No, no existe ninguna ley universal en este terreno
que escapa al determinismo natural. El amor maternal no puede
darse por su- puesto. Es un amor «no incluido». @note

##III. Un ejemplo de una noción de la maternidad como no-obstáculo 

Por último, _Sobre cultura femenina_ de Rosario Castellanos cuestiona la existencia de una cultura propia de las mujeres (una cultura femenina) contrapuesta a lo que se considera cultura masculina. Es decir, todo conocimiento derivado de la creación masculina. En su texto, Castellanos concluye que, en efecto, no hay cultura femenina porque, históricamente, a las mujeres se les ha excluido de la creación cultural. Por lo que ellas han tenido que desarrollar algo propio y equiparable a la creación cultural-masculina: la maternidad. Sin embargo, para este dosier, Castellanos es retomada porque ella ya está constituyendo una noción de maternidad alejada de la que propone Beauvoir. Según ella, los hijos no son un impedimento para la mujer, sino, son lo más propio de su espíritu con respecto de la perpetuación de su ser. 
 
#### Castellanos: _Sobre cultura femenina_
2. La no intervención de la mujer en los procesos culturales puede interpretarse como una indiferencia hacia los valores, pero esta indiferencia tiene su origen, según nuestra opinión, no en una incapacidad específica femenina para apreciarlos (lo que tornaría inexplicable la conducta, aunque no sea más que excepcional  y escasamente relevante, de algunas mujeres que sí han intervenido en los procesos culturales, que no han sido indiferentes para los valores), sino en una falta de interés hacia ellos, emanada no de la inexistencia de una necesidad (la de eternizarse) sino de la posibilidad de satisfacer en otra forma dicha necesidad. 

3. La mujer satisface su necesidad de eternizarse por medio de la maternidad y perpetúa, a través de ella, la vida en el cuerpo, el cuerpo sobre la tierra. 

4. La maternidad es un instinto capaz de transformarse en un sentimiento consciente y, cuando por motivos físicos, psicológicos o sociales no es correctamente ejercitado,  provoca en el sujeto una tentativa de compensación en otro terreno que es, por imitación y por falta de otras alternativas y la carencia de una perspectiva mejor, el de la cultura. @note
 
Samantha Lorena Camacho Jardón {.derecha .espario-arriba1}

## Bibliografía

Badinter, Elisabeth. 1981. ¿Existe el amor maternal? Historia
del amor maternal. Siglos XVII al XX, Barcelona: Paidós/Pomaire.{.francés}

Giallorenzi, María Laura. 2018. “La maternidad como proyecto
individual y autónomo. El caso de las madres solas por elección”,
Journal de Ciencias Sociales, Año 6, No. 11.{.francés}

Morton, Stephen. 2010. “Las mujeres del «tercer mundo» y el pensamiento
occidental”, La manzana de la discordia, Vol. 5, No. 1, enero-junio.{.francés}

Spivak, Gayatri Chakravorty. 2003.“¿Puede hablar el subalterno?”
Revista Colombiana de Antropología, Vol. 39, enero-diciembre.{.francés}

Zerilli, Linda M.G. 1992. “A Process without a Subject: Simone de Beauvoir and Julia Kristeva on Maternity”, Sings, Vol. 18, No. 1, otoño. {.francés}
# Teoría Crítica y Epistemología

##Introducción 

A lo largo de la historia de la filosofía se ha tratado el problema
de la epistemología que estudia el conocimiento y la creencia
justificada. Los principales cuestionamientos han abierto una
serie de divisiones dentro del mismo problema, por ejemplo el
internalismo, externalismo, la evidencia, la verdad, la justificación
ontológica frente a la deontológica, la razón, el escepticismo,
el contextualismo, entre otros. Sin embargo, mi interés primordial
versa sobre la epistemología social y feminista donde se cuestionan
las estructuras mediante las cuales que se asume el conocimiento
como claro y distinto; es decir, que de alguna manera crítica
una visión de la epistemología tradicional individualista pues
hay un énfasis en analizar las prácticas cognitivas para dar
pautas de comprensión respecto a cómo se construye el conocimiento
en relación con el poder o mejor dicho a analizar y observar
al conocimiento como un proceso complejo, pues muchas veces las
nociones consideradas como verdaderas o como “universales” refieren
a una atribución específica de significación ontológicamente
ligada a una biología. Es decir, hay una imagen del mundo científica
que rastrea sus inicios a la Ilustración.

La imagen científica del mundo, que es legado de la Ilustración,
ya ha sido criticada de muchas maneras, particularmente desde
la Teoría Crítica asociada a la Escuela de Frankfurt y firmada
por Horkheimer y Adorno entre otros. Sus principales argumentos
versan sobre una crítica a la razón instrumental a la cual se
alude al antagonismo entre el yo (espíritu) y la naturaleza;
es decir, enuncian la reducción de una noción a la otra. Por
lo tanto, en el sujeto está presente la necesidad de controlar
la naturaleza, lo cual condiciona la estructura y forma de su
pensamiento. El argumento ilustrado deriva en normas cognitivas
que implican la producción de ciertas identidades en las que
el individuo posee una _razón enferma_. Lo anterior lo enuncia
en el libro la _Crítica de la razón instrumental_ donde en el
último capítulo “A propósito del concepto de filosofía” pone
en juego el pensar filosófico frente o dentro de una razón enferma
en donde esta latente la pregunta por la posibilidad de una emancipación.
“Si quisiéramos hablar de una enfermedad que se apodera de la
razón, no debería entenderse esa enfermedad como si hubiese
atacado a la razón en algún momento histórico, sino como algo
inseparable de la esencia de la razón dentro de la civilización,
tal como hasta ahora la hemos conocido. La enfermedad de la razón
tiene sus raíces en su origen, en el deseo del hombre de dominar
la naturaleza, y la convalecencia depende de una comprensión
profunda de la esencia de la enfermedad original, y no de una
curación de los síntomas posteriores”@note. Lo cual visibiliza
la dificultad de separarse de la homogeneización de la sociedad
en la que sutilmente el individuo ha aceptado una cierta noción
de civilización que concibe al ser humano como dominador y objetivador
de la naturaleza, lo cual ha desencadenado una enfermedad, la
cual Horkheimer afirma como originaria que implica hacer un uso
de la razón. Hay una expansión de la enfermedad y una propagación
de sus síntomas. Tratar de comprender la esencia de la enfermedad
implica un rastreo histórico ante lo que entendemos como civilización
y como naturaleza humana, teniendo en cuenta el enfoque materialista
que ha sido un constante. La razón, como tal, se ha convertido
en un instrumento de dominio de la naturaleza humana ante lo
cual hay una problemática, pues en el mero objeto, no hay manera
de pensar en los fines o en una emancipación. Es decir, “la contemplación
calculadora del mundo como presa” ha hecho que la misma teoría
bajo la que la modernidad se regocija y a la que alude, este
fundada en gran medida en irracionalidad. En este sentido, la
misma lógica o cualquier intento de teorizar o argumentar queda
atrapado en la irracionalidad, pues habla desde la enfermedad,
desde una locura que persigue objetivos. De la argumentación
del filósofo alemán, se podría inferir que no hay posibilidad
de emancipación; sin embargo, en su obra no hay una afirmación
tajante de ese tipo, más bien me parece que hay una posibilidad
de emancipación en tanto que Horkheimer la postula como una pregunta
que está latente en todo momento, que no está cerrada en una
solución.

Debido a que la subjetividad está orientada a la autoconservación,
este termina siendo su único fin. Por lo tanto, la razón trabaja
en contra de sí misma y cae en la irracionalidad. Esta afirmación
y el análisis hecho en la “Crítica de la razón instrumental”
se inserta en el debate contra el pragmatismo de la época en
el que hay una crítica a la racionalidad formal y al método cartesiano.
Sin embargo, a partir de estos argumentos yo sostengo que para
dimensionar los alcances de esa crítica y para elaborar sus implicaciones
y efectos transformadores hace falta leer más allá de la escuela
de Frankfurt, se necesita tomar a la Teoría Crítica en un sentido
más amplio en el que críticas feministas, neo-coloniales y de
raza puedan entenderse bajo el espectro de teoría crítica y dialoguen
con esta idea de emancipación y de lógica mercantil. Debido a
que, la emancipación no se reduce a afirmar o a proponer como
posible sino a diagnosticar la complejidad de ciertas estructuras
es necesario dar cuenta de algunas suposiciones epistemológicas
que perpetúan formas de opresión social que se basan en un modelo
estructural mercantil. Específicamente en este dossier deseo
aportar a la comprensión general del tema que un problema epistemológico
debe ser entendido como un problema por el poder. Hay relaciones
e identidades que siempre están en constante cambio por lo que
es necesario estar cuestionando los sustratos epistémicos y explicitando
lo implicito.

El presente dossier está compuesto de seis artículos, divididos
en dos partes que en las que busco esclarecer la relación entre
conocimiento y poder, pues los artículos muestran distintas formas
de analizar o vislumbrar la relación entre lo político y lo epistemológico,
entre el poder y el conocimiento. Como mencione anteriormente
el eje rector que atraviesa a la mayoría de los artículos es
en parte el proyecto de la Teoría Crítica en la que hay un trabajo
por traer la conciencia del vínculo entre la producción social
del conocimiento y la producción social de la sociedad. Es el
conocimiento en donde encuentro que se refleja la razón y comprensión
humana que nos deja entrever la referencia a lo que Horkheimer
llamaría productos, no objetos. Por lo tanto, en mi dossier se
podrá ver que hay ciertas geopolíticas del conocimiento situadas
que someten y oprimen. En este sentido, la tarea de emancipación
es sumamente compleja en tanto que este sometimiento se está
dando desde una manera de comprender la realidad, de relacionarse
con el objeto y con sujetos, desde un modelo de modernidad o
de capitalismo neoliberal que ha hipostasiado todos los ámbitos
bajo una lógica mercantil.

### Comprensión e implicaciones de la razón en la modernidad   

En la primera sección, la cual titule Teoría Crítica, se pueden
encontrar tres artículos que hacen alusión a conceptos claves
de la Escuela de Frankfurt, aunque es específico mencionar que
a los autores a quienes me estaré refiriendo principalmente son
T. Adorno y M. Horkheimer puesto que dentro de la escuela alemana
aunque hay cierta línea que se sigue hay posturas que se diferencian.
El mismo Adorno y Horkheimer discrepan en ciertas nociones. Sin
embargo, para mostrar la comprensión de la visión del mundo me
parecen primordiales. Mi objetivo general en esta sección es
proveer al lector de la perspectiva epistemológica de ambos autores,
tomando en cuenta la denuncia a la modernidad que ambos hacen
y las implicaciones que eso tiene para la razón. En esta sección
los primeros dos artículos: _La modernidad revelada o la crítica
ante la irrenunciabilidad al progreso en Adorno y Horkheimer_@note
escrito por Fernando J. Vergara Herníquez y _The topicality of
Horkheimer and Adorno´s “Elements of Anti-Semitism”_@note escrito
por Stephanie Graf, versan sobre nociones elementales en el pensamiento
de Adorno y Horkheimer. En donde se muestran las implicaciones
de tener una racionalidad a una escala global en donde hay un
análisis de la relación entre poder y capitalismo tardío como
opresión, pues en los artículos se trata de mostrar que hay una
estructura mental, una reacción desviada, en la que hay una capacidad
cognitiva elaborada y delirante. Es bajo este argumento que se
postula que la unidad es el núcleo de un conocimiento opresor
y aliado a la dominación; es decir, al desmantelar la aparente
unidad de sentido se devela a la unidad como mera proyección
que se da en la relación sujeto-objeto. La razón por la cual
encuentro estos dos artículos primordiales es que en ambos se
puede encontrar una oposición a la forma hegemónica de producción
de conocimiento que es concebida como sistematización y totalizante.

Por un lado, Vergara anhonda en la crítica a la razón instrumental
en la modernidad que la Escuela de Frankfurt hizo, haciendo visible
la complejidad de las relaciones dentro del sistema moderno que
se basa en una razón tecno-científica en la que se ve por los
medios y no los fines. Ilustra la imagen que se tiene del mundo
y las implicaciones alrededor de esa imagen.

Por otro lado, el artículo The topicality of Horkheimer and Adorno´s
“Elements of Anti-Semitism” tiene que ver con mi interés por
su segundo apartado dentro del artículo: “Experiencia y proyección.
La dimensión epistemológica en los Elementos del antisemitismo
en las tesis VI y VII”, en tanto que Graf ahonda en la problemática
de los procesos cognitivos en los que hay una suspensión de la
autoreflexión y se cosifica al pensamiento. Me parece elemental
que el artículo vislumbre y explique de una manera clara y concisa
el problema sujeto-objeto desde Adorno y Horkheimer. Asimismo,
me parece que el carácter del antisemitismo permite ilustrar
la complejidad que subsume a este momento histórico.

El tercer artículo de este apartado, aunque no utiliza como autor
principal a algún individuo de la Escuela de Frankfurt, hace
uso de un concepto elemental para poder comprender la Teoría
Crítica, la emancipación. _Emancipación sin Utopía: Sometimiento,
modernidad y las reivindicaciones normativas de la teoría crítica
feminista_@note escrito por la filósofa Amy Allen muestra la
relación entre las relaciones de poder y la emancipación. El
argumento versa sobre el cuestionamiento de si es posible la
emancipación o si esta es compatible con un análisis diagnóstico-explicativo
sobre la dominación del género, explicitando la relación de esto
con la raza, la clase, el sexo y el imperio. Al preguntar lo
anterior, la autora afirma que es necesario comprender una emancipación
negativista pues es a partir de esto que se puede hacer crítica,
en tanto que se concibe en dos sentidos interrelacionados. Integro
este artículo a mi dossier porque la emancipación es un concepto
central en el proyecto de la Teoría Crítica que constantemente
se pone en tensión. Esta idea es fundamental para comprender
e hilar distintos otros artículos del dossier que tienen implícitamente
esta idea de emancipación. Mi interés versa sobre el apartado
“Emancipación sin Utopía” en el que la autora sugiere que la
crítica contemporánea de la Escuela de Frankfurt es demasiado
utópica y no lo suficientemente utópica. Es decir, Hay una crítica
a los autores contemporáneos de la Escuela de Frankfurt. Sin
embargo, me gustaría que el lector pudiese vislumbrar que es
lo que Horkheimer y Adorno, la primera generación de la Escuela
de Frankfurt, entendían como emancipación.

### ¿Es la epistemología un problema por el poder?

En la segunda sección, Epistemología, el lector podrá encontrar
tres artículos: _The dynamics of Power and Knowledge in Science_@note
escrito por el filósofo Joseph Rouse; el capítulo escrito por
la filósofa latinoamericana Linda Alcoff “Epistemologies of Ignorance:
Three Types”@note ; y, _Colonialismo, miradas fronterizas y desnaturalización
de los sustratos Epistemológicos del eurocentrismo_@note escrito
por Daniel Tiapa-Blanco. Todos los artículos en esta sección
tienen como punto en común el análisis de la comprensión de una
epistemología que no puede ser entendida al margen del poder.
Esta relación se hace más clara en el primer artículo The dynamics
of Power and Knowledge in Science, donde Rouse trata una manera
alternativa de comprender la relación entre poder y conocimiento,
afirmando como argumento principal que los problemas de orden
epistemológicos son problemas de poder político, irreductibles
uno a otro e inseparables, pues para entender las relaciones
de poder y tomar acción es necesario tomar en cuenta cómo se
constituye el conocimiento y cómo reinventa las posiciones mediante
las cuales el poder es ejercido. Por lo tanto, no hay unidad
en el conocimiento científico, sino que hay una heterogeneidad
que entiende al poder como dinámico, social y temporalmente situado.
El motivo de la elección de este artículo es que me parece que
propone, ilustra y pone en diálogo la relación entre poder, conocer
y ciencia. En su artículo el autor trata de apostar por una manera
diferente de hacer ciencia en la que primero se tenga clara la
inseparable relación entre poder y saber, pues están interconectadas
y no representan distintos dominios de las cosas, sino que tienen
preocupación acerca de un mismo dominio.

En el segundo artículo Alcoff analiza tres diferentes formas
de ignorancia en tanto que son producidas, sostenidas y tienen
un rol en las prácticas del conocimiento. La autora nota que
el problema de la ignorancia ha sido omitido de las discusiones
y por eso le parece imperante reconocer esta noción como un elemento
clave para analizar lo político y lo epistemológico, reconociendo
a la ignorancia como una práctica epistémica sustantiva en sí
misma, que debe reflexionar y ser crítica de su posición dentro
del sistema económico. Este argumento se relaciona con el análisis
de la primera parte en donde se enuncia una preponderancia de
la lógica mercantil. La autora encuentra entre los análisis de
la ignorancia y la teoría de Horkheimer un punto de relación,
pues hay una hegemonía del mercado en la vida que domina el ámbito
epistemológico. Por lo tanto, hay las consideraciones pragmáticas
juegan un rol en la teoría de las decisiones, las cuales involucran
al mercado; es decir, se hace una referencia a productos de la
praxis humano no a objetos, esto se relaciona con el primer texto
de este apartado en el que se habla de un fetiche por la mercancía
enunciado ya años antes por Marx. Este análisis de las epistemologías
de la ignorancia se inscribe en una época de crisis de racionalidades
que la misma teoría crítica nos permite ver.

Finalmente, el tercer artículo Tiapa-Blanco, muestra en su artículo
un enfoque antropológico, lo cual añade elementos interesantes
a la discusión filosófica tales como la visibilización de una
condición humana “universal”. El principal argumento dentro del
texto es que hay elaboración de discursos que son impuestos como
sustratos epistemológicos de la “modernidad”. Por lo tanto, hay
una propuesta por visibilizar los ejes de los supuestos subyacentes
de los sistemas coloniales y neocoloniales. El presente artículo
fue seleccionado para este dossier porque muestra la narrativa
anónima de la que hace uso el eurocentrismo en donde hay una
fachada de mitos del progreso, de la modernidad que apelan a
invisibilizar la dimensión colonialista y racista de los entramados.
Como el autor menciona, hay una relativización del sentido común
dominante en el pensamiento contemporáneo en donde se usa con
conjunto retórico para mostrar un mundo contemporáneo como representación
de la realidad.

##Lista de contenidos 
### 1. Teoría Crítica 

#### a) _La modernidad revelada o la crítica ante la irrenunciabilidad al progreso en Adorno y Horkheimer_.
“el sujeto racional piensa bajo la figura de principios de corte físico-mate- máticos, que luego se transforman en leyes consideradas naturales, esen- ciales y originarias para la nueva condición racional esquematizada por procedimientos cognoscitivos, éticos y estéticos de corte necesario y universal, persuadiéndolo de poseer una verdad inequívoca, objetiva, positiva; como asimismo, de una apreciación estética definitiva la razón como medida y proporción de la facultad y experiencia originaria y, en fin, de la posesión del bien como norma de comportamiento universal”.@note

“La Teoría Crítica se opone fundamentalmente al modelo racionalista-empirista
de enunciados teóricos predominantes hasta el positivismo lógico
[…]Para Horkheimer, la teoría como estructura de modos de pensar
se ha desarrollado en una sociedad dominada por las técnicas
de producción instrumental e industrial. Tal evaluación tiene
como resultado una retroprogresión del impulso crítico, en
la cual se filtra la formulación mítica de la Ilustración
como fundamento de liberación desmitificadora de la modernidad,
ya que concibe que ni el sujeto ni la historia pueden librarse
del ímpetu míti- co que domina la historia, por ende, el mundo
racionalizado es sólo en apariencia: la autoconciencia del sujeto
dominador de la naturaleza ha desarrollado la convicción de
una posesión desmedida de fuerzas de producción en el reconocimiento
del poder como señores de la naturaleza”.@note

“La denuncia de Adorno y Horkheimer no es respecto la Ilustración,
sino en relación a la perversión de la razón y su substantividad
valórica en razón instrumental, reificadora y cosificadora,
la que hizo olvidar la originaria con- formidad entre naturaleza
y mito, trocándose el dinamismo en unidad de la totalidad como
sentido que orienta los medios necesarios y suficientes para
alcanzar un fin dado”. @note

#### b) _The topicality of Horkheimer and Adorno´s “Elements of Anti-Semitism”_

“En el nivel de la producción de conocimiento, la eliminación
de la diferencia se plasma en la noción moderna de ciencia (que
en el fondo sirvió a la ciencia social hegemónica como modelo)
y que subsume lo particular bajo lo general: culmina en una visión
de conocimiento totalizante que elimina lo no-idéntico. Esa
praxis, generalizada también en el ámbito de las ciencias sociales,
determina la manera hegemónica de acercarse académicamente
a fenómenos sociales como el antisemitismo. La teoría social
ha analizado este fenómeno desde perspectivas históricas, socio-
lógicas, psicoanalíticas y políticas; muchas veces juzgando
prematuramente su propia perspectiva teórica como el origen
del fenómeno. Oponiéndose a esa visión, el esfuerzo teórico
de Adorno y Horkheimer consiste en rodear el fenómeno, mediante
una constelación de tesis que no están conectadas por relaciones
determinantes entre sí. Se puede ver como una especie de collage,
donde se combinan fragmentos para abrir nuevas perspectivas de
sentido. Se trata de una apuesta teórica opuesta al conocimiento
sistematizante”.@note

#### c) _Emancipación sin Utopía: Sometimiento, modernidad y las reivindicaciones normativas de la teoría crítica feminista_ 

“Una concepción de emancipación que es negativista en dos sentidos
interrelacionados —que define la emancipación de forma negativa
como la transformación de un estado de dominación en un campo
móvil y reversible de relaciones de poder, y, por tanto, que
no depende de una visión positivista acerca de una utopía libre
de poder—ofrece el mejor modelo para la teoría crítica feminista
a la luz de las complejidades y ambivalencias del discurso de
la emancipación “.@note

"algo parecido a un modelo de emancipación sin utopía, donde
la emancipación es entendida como libertad o liberación de
un estado de dominación. El ideal normativo que esti- mula la
dimensión de utopía anticipatoria de la crítica aquí es el
ideal negativo de transformar un estado de dominación en un
campo de relaciones de poder móvil y reversible, no una noción
utópica positiva de una sociedad buena completamente libre de
relaciones de poder". @note

###2. Epistemologias 

####a) _The dynamics of power and Knowledge in Science_ 

“We avoid fetishism not by strictly separating the natural and
the social, nor by reducing the natural to the social, but by
rethinking the distinction. Knowledge and Power initiated such
rethinking by extending "power" beyond narrowly "social" interactions
to include transformations of the configuration of practices
and things within which actions are intelligible. Materials,
things, processes, and practices shaped by the construction and
extension of scientific "micro- worlds" play an especially important
role in such configurations, and hence in the alignment of power.
Understanding power in this way neither reifies social relations,
nor ascribes interests, values, rights, or a "natural" order
to things. It does, however, show that distinctions between the
"natural world" and the "social world" cannot legitimately confine
power to the latter”.@note

#### b) "Epistemologies of Ignorance: Three Types" 

“This idea that the “perceiving organ” has a historical character
led to Horkheimer’s proposal that the crucial task for critical
theorists is to denaturalize both the product and the process
of knowledge. He argued, thus, that we need to analyze the social
production of the “knowing individual as such” (Horkheimer 1975,
199). If we accept the idea that both the object perceived and
the perceiving organ are socially and politically preformed,
then we might begin to think of ignorance as the result of a
historically specific mode of knowing and perceiving”. @note

“Analyzing ignorance will require not only an analysis of the
general conditions of epistemic situatedness, the epistemic resources
distributed differently across social locations, or the structural
contexts that organize and reproduce oppression; to truly understand
the cause of the problem of ignorance, we also need to make epistemology
reflexively aware and critical of its location within an economic
system”. @note

#### c) _Colonialismo, miradas fronterizas y desnaturalización de los sustratos Epistemológicos del eurocentrismo_

"A partir del proceso imaginario mediante el cual toda cultura
naturaliza sus referentes inmediatos de existencia, estos sistemas,
impuestos por medio de la acción explícita de agencias históricas,
no sólo se naturalizan, sino que se difuminan al volverse la
expresión de lo universal. Así, en la experiencia histórica
de configuración del lugar de enunciación colonizante, la imagen
de la universalidad logró su máximo punto de abstracción sobre
la base de la idea de que este lugar no existe como forma concreta,
sino como un punto in-observado de observación, como una conciencia
supra-terrenal con capacidades comparables a la deidad (Castro-Gómez,
2007: 83). De este modo, la expresión misma de que una práctica
histórico-cultural, entre otras posibles, sea entendida a partir
de la imagen del “uni-verso”,da cuenta de su profunda raíz mítica".
@note

Sofía Medina Vázquez {derecha .espacio-arriba1}

## Rerencias 

Allen, Amy. 2016. Emancipación sin utopía: Sometimiento, modernidad
y las reivindicaciones normativas de la teoría crítica feminista.
Revista _Signos Filosóficos_, vol. XVIII, núm. 35 (enero-junio):
170-196.{.francés}

Graf, Stephanie. 2017. La actualidad de los “Elementos del Antisemitismo”
de Adorno y Horkheimer. Revista _Signos Filosóficos_, vol.XIX,
núm. 38 (julio-diciembre): 118-149. {.francés}

Horkheimer, Max. 1973. _Crítica de la Razón Instrumental_. Buenos
Aires: SUR.

\00artín Alcoff, Linda. 2007. “Epistemologies of Ignorance: Three
types”, en _Race and Epistemologies of Ignorance_, editado por
Sullivan Shannon y Nancy Tuana. E.U.A: State University of New
York 2007.{.francés}

Rouse, Joseph. 1991. The Dynamics of Power an Knowledge in Science.
_The Journal of Philosophy_, vol. 88, núm. 11 (noviemnre): 658-665.{.francés}

Tiapa-Blanco, Francisco Daniel. 2019. Colonialismo, Miradas fronterizas
y denaturalización de los sustratos epistemológicos del eurocentrismo.
Revista _LiminaR. Estudios Sociales y Humanísticos_ vol. XVII,
núm. 1(enero-junio):114-126.{.francés}

Vergara Henríquez, Fernando J. 2011. La modernidad revelada o
la crítica ante la irrenunciabilidad al progreso en Adorno y
Horkheimer. Revista _Opción_, vol. 27, núm. 65 (septiembre-diciembre):
93-115.{.francés}

Ximena Gabilondo


Dosier
Introducción:

#Título

La pregunta latente que enmarca los materiales de este dosier es, si comprender el vegetarianismo como una opción o preferencia individual
independiente de la filosofía no reproduce las jerarquías especistas que la filosofía crítica tendría que interrogar e incluso combatir hoy en
día. Esta misma pregunta es también el hilo conductor del trabajo final a presentar, el cual no puede lograrse sin el apoyo en los textos,
fragmentos, y autores seleccionados en este dosier, y algunos otros que se desprenden de los mismos. 
 
No podemos hablar de feminismo en singular, sino de feminismos. Dentro de estos feminismos hay distintas posturas, ramas, teorías y más, y es en
esas líneas de donde surge el eco feminismo. Igualmente, el ecofeminismo suele tener una definición general: según el diccionario de Oxford se
trata de un movimiento que trata la dominación del patriarcado sobre la naturaleza como algo indisociable de la dominación hacia las mujeres. Esta
definición abre el diálogo para entender la intrincada relación entre las distintas formas de discriminación, las cuales no deben excluir la
discriminación y dominación de la naturaleza, llámese mundo animal o vegetal, y que son también estas prácticas producto del sistema patriarcal. 

Si bien esta definición es el punto de partida, sigue siendo
una muy general. El eco feminismo también se divide y existen
diversas posturas. Una de estas perspectivas es la que encuentra
en las mujeres una especie de conexión con la naturaleza, como
si el cuidado que dan las mujeres se extendiera también a la
naturaleza. Bajo esta perspectiva, son las mujeres una especie
de cuidadoras de los animales y las plantas, haciendo énfasis
a las teorías que ---- a las mujeres como quienes cuidaban de
los sembradíos, quienes recolectaban las verduras y actividades
afín.

Hay otra perspectiva, la cual este dosier y trabajo retoma: no
se trata de si son las mujeres o los hombres los que deben cuidar
al mundo vegetal y animal, sino que es el sistema patriarcal
el que ha perpetuado las formas de discriminación presentes en
nuestra sociedad, racismo, sexismo, xenofobia, especismo y más
parten de la misma raíz. Esta raíz es la estructura de dominación
patriarcal, la cual toma, consume, y desecha los “objetos” a
su paso, objetos entre comillas pues estos pueden ser también
humanos, animales no humanos, y el mundo vegetal. Paradójicamente,
esta destrucción del Otro es lo que ha nos ha llevado a la crisis
ambiental actual, de la cual nadie tiene escapatoria.

Aunque en los debates feministas actuales, específicamente las
líneas feministas y eco feministas más ad hoc a la postura a
presentar en este trabajo y dosier, se reconoce que la liberación
de las mujeres es indisociable de la liberación de otras formas
de opresión; sin embargo, en la política feminista predominante
de hoy en día, se continúa reproduciendo el sesgo antropocéntrico
de la política occidental al enfatizar el sujeto ‘mujeres’ en
lugar de igualdad entre especies.

Para exponer lo anterior, recurro al término de ‘vegetarianismo’
el cual recientemente ha mutado a ‘veganismo’, pues el vegetarianismo
suele quedarse corto. Este concepto lo defino tras el libro de
Carol J. Adams “La política sexual de la carne” en el cual establece
que el vegetarianismo es una decisión ética que elimina el consumo
de carne de la dieta, productos y entretenimiento pues considera
el hacerlo una explotación injustificable de los animales. El
vegetarianismo asocia el consumo de carne con los valores patriarcales.

El vegetarianismo se ha quedado corto, pues si bien empezó como
una postura que obviaba el no consumir ningún tipo de producto
animal, el hacer énfasis en la carne generó que en los últimos
años se formaran dos puntos, sobre todo en el ámbito alimenticio:
vegetarianos y veganos. Los vegetarianos entonces eliminan cualquier
consumo de carne animal, pero pueden incorporar el uso de productos
lácteos y huevos y suelen solo enfocarse en su consumo de alimentos,
adoptando una dieta “basada en plantas”; los veganos eliminan
cualquier tipo de opresión y sufrimiento animal, que permea el
no comer ningún tipo de producto que provenga de un animal, así
como tampoco promover su explotación al comprar, consumir o visitar
lugares, productos, empresas que los lastimen o utilicen de alguna
manera.

El veganismo, al ser una postura ética y filosófica, está ligada
al activismo. De aquí surge el siguiente concepto: el especismo
(o especiesimo en algunas traducciones). El veganismo es antiespecista.
Para hablar del especismo retomo un fragmento del artículo de
Sandra Baquedano Jer “Jerarquías especistas en el pensamiento
occidental” la cual traza el primer uso de este concepto hacia
1971 en una denuncia de los experimentos que se le hacían a los
animales en los laboratorios. Tal como el racismo proviene de
raza, el sexismo de sexo, y más, el especismo proviene de especie.
Alude a un tipo de discriminación que el humano ejerce sobre
otros seres vivos, otras especies no humanas. Esta discriminación
y dominación está sustentada en distintos pensamientos que han
normalizado en situar al ser humano como un ser superior ante
las demás especies, y que éstas otras están a su disposición.

A fin de esclarecer y evidenciar las dimensiones filosóficas
del activismo vegetariano resulta útil sumergirnos en distintos
autores que tratan el tema de especismo y vegetarianismo / veganismo.
De ahora en adelante, cuando hable de ‘vegetarianismo’ me estoy
refiriendo al vegetarianismo inicial, el cual elimina cualquier
tipo de producto animal de la dieta y cualquier otro tipo de
consumo. Es decir, a lo que ahora coloquialmente se le llama
‘veganismo’. En primer lugar aparece Peter Singer con su obra
de 1975 “liberación animal”. Si bien no fue elprimer pensador
en tratar el tema, si es el primero en generar suficiente diálogo
y polémica para que entrara en las discusiones filosóficas del
momento y hasta la actualidad.

Val Plumwood

Sandra Baquedano


Fragmentos seleccionados

Definir los términos y conceptos a tratar

Eco feminismo A movement in which mankind's unfortunate domination
over nature is seen as analogous to mens' equally unfortunate
domination over women—an alliance that adds resonance both to
ecological and feminist concerns.


Vegetarianismo “[The] self-conscious omission of meat because
of ethical vegetarianism, that is, vegetarianism arising from
an ethical decision that regards meat eating as an unjustifiable
exploitation of the other animals. [...] The vegetarian quest
consists of: the revelation of the nothingness of meat, naming
the relationships one sees with animals, and finally, rebuking
a meat eating and patri- archal world.


Especismo / especiecismo
Aunque este problema tenga un origen prehistórico, el concepto speciesism surgió solo en 1971 y se lo debemos a Richard Ryder cuando acuñó el
término en su artículo Experiments on Animals en vista de denunciar los crueles experimentos que se realizaban en los laboratorios con animales.
(p. 79). Ahí señala que especismo proviene de especie, así como racismo de raza. Este último se produce a nivel de intraespecie, mientras que el
primero supone el traspaso de ella. No se trata de una igualdad o semejanza sus-tancial entre ambos fenómenos sino de un análogo referencial,
pero en uno y otro caso la comparación alude a una discriminación. En lo que concierne al especismo, aquella que ejerce el ser humano contra un
sinnúmero de seres vivos no humanos, basada precisamente en la pertenencia a una especie. Esta forma de discriminación se aplica en general a
través de la creencia que afirma la superioridad de una especie en detrimento de las demás y preconiza, entre otras cosas, la separación de
especies o grupos por segregación en condiciones de vulnerabilidad.  


Perspectivas

Peter Singer “Liberación animal”

"Entre los factores que dificultan la tarea de provocar el interés
público por los animales destaca, como uno de primer orden, el
supuesto de que «los humanos están primero» y que cualquier problema
relativo a los animales no puede ser comparable a los problemas
de los hombres, tanto por su seriedad moral como por la importancia
política del tema. Sobre este supuesto debe hacerse una serie
de puntualizaciones. Primero, es en sí mismo una muestra de especismo.
¿Cómo puede saber nadie, sin antes haber hecho un estudio detallado,
que el problema es menos serio que los problemas relativos al
sufrimiento humano? Únicamente se estaría en posición de defender
esta tesis si se asume que los animales realmente no importan
y que, aunque sufran mucho, su sufrimiento es menos importante
que el de los humanos. Pero el dolor es el dolor, y la importancia
de evitar el dolor y el sufrimiento innecesarios no disminuye
porque el ser afectado no sea un miembro de nuestra especie.
¿Qué pensaríamos si alguien nos dijera que «los blancos están
primero» y que, por tanto, la pobreza en África no plantea un
problema tan serio como la pobreza en Europa?"


Val Plumwood “Feminism and the mastery of nature” 

“Women have faced an unacceptable choice within patriarchy with
respect to their ancient identity as nature. They either accept
it (naturalism) or reject it (and endorse the dominant mastery
model). Attention to the dualistic problematic shows a way of
resolving this dilemma. Women must be treated as just as fully
human and as fully part of human culture as men. But both men
and women must challenge the dualised conception of human identity
and develop an alternative culture which fully recognises human
identity as continuous with, not alien from, nature. The dualised
conception of nature as inert, passive and mechanistic would
also be challenged as part of this development.”

Sandra Baquedano “Jerarquías especistas en el pensamiento occidental”
La situación contemporánea no es solo de destrucción del entorno,
sino de jerarquías especistas que nacen en el interior del ser
humano y que externamente dejan también al descubierto el complejo
de la autodestrucción, ya que el daño a la naturaleza, al resto
de las formas de vida, puede ser considerado una forma de autodestrucción
en cuanto involucra al individuo y por exten- sión también
a la especie, tratándose de una destrucción activa del entorno
natural necesario tanto para la vida del ser humano como para
la preservación de la biodiversidad.

Debates Angélica Velasco Sesma “La ética animal: una cuestión
feminista?”


## Bibliografía / Referencias
