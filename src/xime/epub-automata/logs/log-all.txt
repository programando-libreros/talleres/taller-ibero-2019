Pecas: 2019.11.09-7e451d5
 Ruby: 2.6.4-p104
 Host: x86_64-pc-linux-gnu

Software bajo licencia GPLv3+: <https://gnu.org/licenses/gpl.html>.
Documentación: <http://pecas.cliteratu.re/>.

------------------------------------------------------------------

# pc-pandog
$ ruby /home/ted/.pecas/epub/automata/../../base-files/pandog/pandog.rb -i /home/ted/Repositorios/taller-ibero/src/xime/archivos-madre/md/xime.md -o /home/ted/Repositorios/taller-ibero/src/xime/epub-automata/.xime.xhtml

# pc-creator
$ ruby /home/ted/.pecas/epub/automata/../creator/creator.rb -o epub       

# pc-divider
$ ruby /home/ted/.pecas/epub/automata/../divider/divider.rb -f /home/ted/Repositorios/taller-ibero/src/xime/epub-automata/.xime.xhtml -d epub/OPS/xhtml -s epub/OPS/css/styles.css -i 3 

# pc-recreator
$ ruby /home/ted/.pecas/epub/automata/../recreator/recreator.rb -d epub -y /home/ted/Repositorios/taller-ibero/src/xime/epub-automata/meta-data.yaml 

# pc-changer
$ ruby /home/ted/.pecas/epub/automata/../changer/changer.rb -e /home/ted/Repositorios/taller-ibero/src/xime/epub-automata/epub-38233cce.epub --version 3.0.0

# pc-analytics
$ ruby /home/ted/.pecas/epub/automata/../../base-files/analytics/analytics.rb -f /home/ted/Repositorios/taller-ibero/src/xime/epub-automata/epub-38233cce.epub --json 

# epubcheck 4.0.2
$ java -jar /home/ted/.pecas/epub/automata/../../src/alien/epubcheck/4-0-2/epubcheck.jar epub-38233cce.epub -out log.xml -q

  <?xml version="1.0" encoding="UTF-8"?>
  <jhove xmlns="http://hul.harvard.edu/ois/xml/ns/jhove"
         date="2016-11-29"
         name="epubcheck"
         release="4.0.2">
     <date>2019-11-20T13:27:22-06:00</date>
     <repInfo uri="epub-38233cce.epub">
        <created>2019-11-20T13:27:17Z</created>
        <lastModified>2019-11-20T13:27:17Z</lastModified>
        <format>application/epub+zip</format>
        <version>3.0.1</version>
        <status>Well-formed</status>
        <mimeType>application/epub+zip</mimeType>
        <properties>
           <property>
              <name>CharacterCount</name>
              <values arity="Scalar" type="Long">
                 <value>11283</value>
              </values>
           </property>
           <property>
              <name>Language</name>
              <values arity="Scalar" type="String">
                 <value>es</value>
              </values>
           </property>
           <property>
              <name>Info</name>
              <values arity="List" type="Property">
                 <property>
                    <name>Identifier</name>
                    <values arity="Scalar" type="String">
                       <value>1_0_0-38233cce3a1c4b1b8fff87781ce539a8</value>
                    </values>
                 </property>
                 <property>
                    <name>CreationDate</name>
                    <values arity="Scalar" type="Date">
                       <value>2019-11-20T13:27:17Z</value>
                    </values>
                 </property>
                 <property>
                    <name>ModDate</name>
                    <values arity="Scalar" type="Date">
                       <value>2019-11-20T13:27:17Z</value>
                    </values>
                 </property>
                 <property>
                    <name>Title</name>
                    <values arity="Scalar" type="String">
                       <value>Filosofía y vegetarianismo</value>
                    </values>
                 </property>
                 <property>
                    <name>Creator</name>
                    <values arity="Scalar" type="String">
                       <value>Gabilondo, Ximena</value>
                    </values>
                 </property>
                 <property>
                    <name>Publisher</name>
                    <values arity="Scalar" type="String">
                       <value>Ibero</value>
                    </values>
                 </property>
                 <property>
                    <name>Subject</name>
                    <values arity="Scalar" type="String">
                       <value>Filosofía</value>
                    </values>
                 </property>
              </values>
           </property>
           <property>
              <name>Fonts</name>
              <values arity="List" type="Property">
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Alegreya Regular</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Alegreya Italic</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Alegreya Bold</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Alegreya BoldItalic</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
              </values>
           </property>
           <property>
              <name>MediaTypes</name>
              <values arity="Array" type="String">
                 <value>application/x-dtbncx+xml</value>
                 <value>application/xhtml+xml</value>
                 <value>text/css</value>
                 <value>application/vnd.ms-opentype</value>
              </values>
           </property>
        </properties>
     </repInfo>
  </jhove>

# epubcheck 3.0.1
$ java -jar /home/ted/.pecas/epub/automata/../../src/alien/epubcheck/3-0-1/epubcheck.jar epub-38233cce_3-0-0.epub -out log.xml -q

  <?xml version="1.0" encoding="UTF-8"?>
  <jhove xmlns="http://hul.harvard.edu/ois/xml/ns/jhove" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="epubcheck" release="3.0.1" date="2013-05-10">
   <date>2019-11-20T13:27:24-06:00</date>
   <repInfo uri="epub-38233cce_3-0-0.epub">
    <created>2019-11-20T13:27:17Z</created>
    <lastModified>2019-11-20T13:27:17Z</lastModified>
    <format>application/epub+zip</format>
    <version>3.0</version>
    <status>Well-formed</status>
    <mimeType>application/epub+zip</mimeType>
    <properties>
     <property><name>CharacterCount</name><values arity="Scalar" type="Long"><value>11283</value></values></property>
     <property><name>Language</name><values arity="Scalar" type="String"><value>es</value></values></property>
     <property><name>Info</name><values arity="List" type="Property">
      <property><name>Identifier</name><values arity="Scalar" type="String"><value>1_0_0-38233cce3a1c4b1b8fff87781ce539a8</value></values></property>
      <property><name>CreationDate</name><values arity="Scalar" type="Date"><value>2019-11-20T13:27:17Z</value></values></property>
      <property><name>ModDate</name><values arity="Scalar" type="Date"><value>2019-11-20T13:27:17Z</value></values></property>
      <property><name>Title</name><values arity="Scalar" type="String"><value>Filosofía y vegetarianismo</value></values></property>
      <property><name>Creator</name><values arity="Scalar" type="String"><value>Gabilondo, Ximena</value></values></property>
      <property><name>Publisher</name><values arity="Scalar" type="String"><value>Ibero</value></values></property>
     </values></property>
     <property><name>Fonts</name><values arity="List" type="Property">
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Alegreya Regular</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Alegreya Italic</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Alegreya Bold</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Alegreya BoldItalic</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
     </values></property>
    </properties>
   </repInfo>
  </jhove>

# ace
$ ace -o logs/ace epub-38233cce.epub
=> Ace no pudo ser localizado, el EPUB no puede ser verificado…

# kindlegen
$ kindlegen epub-38233cce.epub

  *************************************************************
   Amazon kindlegen(Linux) V2.9 build 1028-0897292 
   A command line e-book compiler 
   Copyright Amazon.com and its Affiliates 2014 
  *************************************************************
  
  Info(prcgen):I1047: Added metadata dc:Title        "Filosofía y vegetarianismo"
  Info(prcgen):I1047: Added metadata dc:Creator      "Gabilondo, Ximena"
  Info(prcgen):I1047: Added metadata dc:Publisher    "Ibero"
  Info(prcgen):I1047: Added metadata fixed-layout    "false"
  Info(prcgen):I1047: Added metadata dc:Subject      "Filosofía"
  Info(prcgen):I1002: Parsing files  0000003
  Warning(htmlprocessor):W28001: CSS style specified in content is not supported by Kindle readers. Removing the CSS property: 'column-count' in file: /tmp/mobi-pWe8K4/OPS/css/styles.css
  Warning(htmlprocessor):W28001: CSS style specified in content is not supported by Kindle readers. Removing the CSS property: 'column-gap' in file: /tmp/mobi-pWe8K4/OPS/css/styles.css
  Warning(htmlprocessor):W28001: CSS style specified in content is not supported by Kindle readers. Removing the CSS property: 'column-rule' in file: /tmp/mobi-pWe8K4/OPS/css/styles.css
  Info(prcgen):I1015: Building PRC file
  Info(prcgen):I1006: Resolving hyperlinks
  Warning(prcgen):W14016: Cover not specified
  Info(pagemap):I8000: No Page map found in the book
  Info(prcgen):I1045: Computing UNICODE ranges used in the book
  Info(prcgen):I1046: Found UNICODE range: Basic Latin [20..7E]
  Info(prcgen):I1046: Found UNICODE range: Latin-1 Supplement [A0..FF]
  Info(prcgen):I1046: Found UNICODE range: General Punctuation - other than Windows 1252 [2015..2017]
  Info(prcgen):I1046: Found UNICODE range: General Punctuation - Windows 1252 [201C..201E]
  Info(prcgen):I1046: Found UNICODE range: Combining Diacritical Marks [300..36F]
  Info(prcgen):I1017: Building PRC file, record count:   0000004
  Info(prcgen):I1039: Final stats - text compressed to (in % of original size):  53.24%
  Info(prcgen):I1040: The document identifier is: "Filosofia_y_vegetarianismo"
  Info(prcgen):I1041: The file format version is V6
  Info(prcgen):I1031: Saving PRC file
  Info(prcgen):I1033: PRC built with WARNINGS!
  Info(prcgen):I1016: Building enhanced PRC file
  Info(prcgen):I1007: Resolving mediaidlinks
  Info(prcgen):I1011: Writing mediaidlinks
  Info(prcgen):I1009: Resolving guide items
  Info(prcgen):I1017: Building PRC file, record count:   0000007
  Info(prcgen):I1039: Final stats - text compressed to (in % of original size):  48.13%
  Info(prcgen):I1041: The file format version is V8
  Info(prcgen):I1032: PRC built successfully
  Info(prcgen):I15000:  Approximate Standard Mobi Deliverable file size :   0000020KB
  Info(prcgen):I15001:  Approximate KF8 Deliverable file size :   0000660KB
=>Info(prcgen):I1037: Mobi file built with WARNINGS!
  
